\begin{thebibliography}{}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi

\bibitem[{{Ackermann} {et~al.}(2015){Ackermann}, {Ajello}, {Albert}, {Atwood},
  {Baldini}, {Ballet}, {Barbiellini}, {Bastieri}, {Bechtol}, \&
  {Bellazzini}}]{Ackermann2015a}
{Ackermann}, M., {Ajello}, M., {Albert}, A., {et~al.} 2015, \apj, 799, 86

\bibitem[{{Ajello} {et~al.}(2008){Ajello}, {Greiner}, {Sato}, {Willis},
  {Kanbach}, {Strong}, {Diehl}, {Hasinger}, {Gehrels}, \&
  {Markwardt}}]{Ajello2008a}
{Ajello}, M., {Greiner}, J., {Sato}, G., {et~al.} 2008, \apj, 689, 666

\bibitem[{{Beacom} \& {Y{\"u}ksel}(2006)}]{Beacom2006a}
{Beacom}, J.~F., \& {Y{\"u}ksel}, H. 2006, \prl, 97, 071102

\bibitem[{{Chiu} {et~al.}(2017){Chiu}, {Boggs}, {Kierans}, {Lowell}, {Sleator},
  {Tomsick}, {Zoglauer}, {Amman}, {Chang}, \& {Chu}}]{Chiu2017a}
{Chiu}, J.~L., {Boggs}, S.~E., {Kierans}, C.~A., {et~al.} 2017, International
  Cosmic Ray Conference, 301, 796

\bibitem[{{Churazov} {et~al.}(2014){Churazov}, {Sunyaev}, {Isern},
  {Kn{\"o}dlseder}, {Jean}, {Lebrun}, {Chugai}, {Grebenev}, {Bravo}, \&
  {Sazonov}}]{Churazov2014a}
{Churazov}, E., {Sunyaev}, R., {Isern}, J., {et~al.} 2014, \nat, 512, 406

\bibitem[{{Diehl} {et~al.}(2014){Diehl}, {Siegert}, {Hillebrandt}, {Grebenev},
  {Greiner}, {Krause}, {Kromer}, {Maeda}, {R{\"o}pke}, \&
  {Taubenberger}}]{Diehl2014a}
{Diehl}, R., {Siegert}, T., {Hillebrandt}, W., {et~al.} 2014, Science, 345,
  1162

\bibitem[{{Horiuchi} \& {Beacom}(2010)}]{Horiuchi2010a}
{Horiuchi}, S., \& {Beacom}, J.~F. 2010, \apj, 723, 329

\bibitem[{{Komura} {et~al.}(2017){Komura}, {Takada}, {Mizumura}, {Miyamoto},
  {Takemura}, {Kishimoto}, {Kubo}, {Kurosawa}, {Matsuoka}, \&
  {Miuchi}}]{Komura2017a}
{Komura}, S., {Takada}, A., {Mizumura}, Y., {et~al.} 2017, \apj, 839, 41

\bibitem[{{Krivonos} {et~al.}(2015){Krivonos}, {Tsygankov}, {Lutovinov},
  {Revnivtsev}, {Churazov}, \& {Sunyaev}}]{Krivonos2015a}
{Krivonos}, R., {Tsygankov}, S., {Lutovinov}, A., {et~al.} 2015, \mnras, 448,
  3766

\bibitem[{{Kurosawa} {et~al.}(2010){Kurosawa}, {Kubo}, {Hattori}, {Ida},
  {Iwaki}, {Kabuki}, {Kubo}, {Kunieda}, {Miuchi}, \&
  {Nakahara}}]{Kurosawa2010a}
{Kurosawa}, S., {Kubo}, H., {Hattori}, K., {et~al.} 2010, Nuclear Instruments
  and Methods in Physics Research A, 623, 249

\bibitem[{{Mizumura} {et~al.}(2018){Mizumura}, {Takada}, \&
  {Tanimori}}]{Mizumura2018a}
{Mizumura}, Y., {Takada}, A., \& {Tanimori}, T. 2018, arXiv e-prints,
  arXiv:1805.07939

\bibitem[{Mizumura {et~al.}(2014)Mizumura, Tanimori, Kubo, Takada, Parker,
  Mizumoto, Sonoda, Tomono, Sawano, Nakamura, Matsuoka, Komura, Nakamura, Oda,
  Miuchi, Kabuki, Kishimoto, Kurosawa, \& Iwaki}]{Mizumura2014a}
Mizumura, Y., Tanimori, T., Kubo, H., {et~al.} 2014, Journal of
  Instrumentation, 9, C05045

\bibitem[{{Nakamura} {et~al.}(2018){Nakamura}, {Tanimori}, {Takada},
  {Mizumura}, {Komura}, {Kishimoto}, {Takemura}, {Yoshikawa}, {Tanigushi}, \&
  {Onozaka}}]{Nakamura2018a}
{Nakamura}, Y., {Tanimori}, T., {Takada}, A., {et~al.} 2018, in Society of
  Photo-Optical Instrumentation Engineers (SPIE) Conference Series, Vol. 10699,
  \procspie, 106995W

\bibitem[{{Sch{\"o}nfelder}(2004)}]{Schoenfelder2004a}
{Sch{\"o}nfelder}, V. 2004, \nar, 48, 193

\bibitem[{{Sch{\"o}nfelder} {et~al.}(1993){Sch{\"o}nfelder}, {Aarts},
  {Bennett}, {de Boer}, {Clear}, {Collmar}, {Connors}, {Deerenberg}, {Diehl},
  \& {von Dordrecht}}]{Schoenfelder1993a}
{Sch{\"o}nfelder}, V., {Aarts}, H., {Bennett}, K., {et~al.} 1993, \apjs, 86,
  657

\bibitem[{{Siegert} {et~al.}(2016){Siegert}, {Diehl}, {Khachatryan}, {Krause},
  {Guglielmetti}, {Greiner}, {Strong}, \& {Zhang}}]{Siegert2016a}
{Siegert}, T., {Diehl}, R., {Khachatryan}, G., {et~al.} 2016, \aap, 586, A84

\bibitem[{{Takada} {et~al.}(2011){Takada}, {Kubo}, {Nishimura}, {Ueno},
  {Hattori}, {Kabuki}, {Kurosawa}, {Miuchi}, {Mizuta}, \&
  {Nagayoshi}}]{Takada2011a}
{Takada}, A., {Kubo}, H., {Nishimura}, H., {et~al.} 2011, \apj, 733, 13

\bibitem[{{Takada} {et~al.}(2016){Takada}, {Tanimori}, {Kubo}, {Mizumoto},
  {Mizumura}, {Komura}, {Kishimoto}, {Takemura}, {Yoshikawa}, \&
  {Nakamasu}}]{Takada2016a}
{Takada}, A., {Tanimori}, T., {Kubo}, H., {et~al.} 2016, in Society of
  Photo-Optical Instrumentation Engineers (SPIE) Conference Series, Vol. 9905,
  \procspie, 99052M

\bibitem[{{Tanimori} {et~al.}(2015){Tanimori}, {Kubo}, {Takada}, {Iwaki},
  {Komura}, {Kurosawa}, {Matsuoka}, {Miuchi}, {Miyamoto}, \&
  {Mizumoto}}]{Tanimori2015a}
{Tanimori}, T., {Kubo}, H., {Takada}, A., {et~al.} 2015, \apj, 810, 28

\bibitem[{{Tanimori} {et~al.}(2017){Tanimori}, {Mizumura}, {Takada},
  {Miyamoto}, {Takemura}, {Kishimoto}, {Komura}, {Kubo}, {Kurosawa}, \&
  {Matsuoka}}]{Tanimori2017a}
{Tanimori}, T., {Mizumura}, Y., {Takada}, A., {et~al.} 2017, Scientific
  Reports, 7, 41511

\bibitem[{{Tomono} {et~al.}(2017){Tomono}, {Mizumoto}, {Takada}, {Komura},
  {Matsuoka}, {Mizumura}, {Oda}, \& {Tanimori}}]{Tomono2017a}
{Tomono}, D., {Mizumoto}, T., {Takada}, A., {et~al.} 2017, Scientific Reports,
  7, 41972

\bibitem[{Tsurutani {et~al.}(2018)Tsurutani, Park, Falkowski, Lakhina, Pickett,
  Bortnik, Hospodarsky, Santolik, Parrot, Henri, \& Hajra}]{Tsurutani2018a}
Tsurutani, B.~T., Park, S.~A., Falkowski, B.~J., {et~al.} 2018, Journal of
  Geophysical Research: Space Physics, 123, 10,009

\bibitem[{{Ueno} {et~al.}(2011){Ueno}, {Tanimori}, {Kubo}, {Miuchi}, {Kabuki},
  {Iwaki}, {Higashi}, {Parker}, {Kurosawa}, \& {Takahashi}}]{Ueno2011a}
{Ueno}, K., {Tanimori}, T., {Kubo}, H., {et~al.} 2011, Nuclear Instruments and
  Methods in Physics Research A, 628, 158

\end{thebibliography}
