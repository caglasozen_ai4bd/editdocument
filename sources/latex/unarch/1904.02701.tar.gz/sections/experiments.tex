% !TEX root = ../1128.tex

\section{Experiments}
\subsection{Dataset and Evaluation Metrics}
All experiments are implemented on the challenging MS COCO~\cite{coco} dataset.
It consists of 115k images for training (\emph{train-2017}) and 5k images for validation (\emph{val-2017}).
There are also 20k images in \emph{test-dev} that have no disclosed labels.
We train models on \emph{train-2017}, and report ablation studies and final results on \emph{val-2017} and \emph{test-dev} respectively.
All reported results follow standard COCO-style Average Precision (AP) metrics that include
AP (averaged over IoU thresholds), AP$_{50}$ (AP for IoU threshold 50\%), AP$_{75}$ (AP for IoU threshold 75\%).
We also include AP$_{S}$, AP$_M$, AP$_L$, which correspond to the results on small, medium and large scales respectively.
The COCO-style Average Recall (AR) with AR$^{100}$, AR$^{300}$, AR$^{1000}$ correspond to the average recall when there are 100, 300 and 1000 proposals per image respectively.

% Implementation Details
\subsection{Implementation Details}
For fair comparisons, all experiments are implemented on PyTorch~\cite{pytorch} and mmdetection~\cite{mmdetection2018}.
The backbones used in our experiments are publicly available.
We train detectors with 8 GPUs (2 images per GPU) for 12 epochs with an initial learning rate of 0.02, and decrease it by 0.1 after 8 and 11 epochs respectively if not specifically noted.
All other hyper-parameters follow the settings in mmdetection~\cite{mmdetection2018} if not specifically noted.

% overall ablation
\begin{table*}[htb]
	\centering
	\caption{Effects of each component in our Libra R-CNN. Results are reported on COCO \emph{val-2017}.}
	\vspace{0.1cm}
	\addtolength{\tabcolsep}{0pt}
	\begin{tabular}{*{12}{c}}
		\toprule
		IoU-balanced Sampling & Balanced Feature Pyramid & Balanced L1 Loss & AP   & $\text{AP}_{50}$ & $\text{AP}_{75}$ & $\text{AP}_{S}$ & $\text{AP}_{M}$ & $\text{AP}_{L}$ \\
		\midrule
		                      &                          &                  & 35.9 & 58.0             & 38.4             & 21.2            & 39.5            & 46.4            \\
		\checkmark            &                          &                  & 36.8 & 58.0             & 40.0             & 21.1            & 40.3            & 48.2            \\
		\checkmark            & \checkmark               &                  & 37.7 & 59.4             & 40.9             & 22.4            & 41.3            & 49.3            \\
		\checkmark            & \checkmark               & \checkmark       & 38.5 & 59.3             & 42.0             & 22.9            & 42.1            & 50.5            \\
		\bottomrule
	\end{tabular}
	\label{tab:overall-ablation}
\end{table*}


% rpn
\begin{table}[htb]
	\centering
	\caption{Comparisons between Libra RPN and RPN. The symbol ``*'' means our re-implements.}
	\vspace{-2pt}
	\addtolength{\tabcolsep}{-5pt}
	\begin{tabular}{*{12}{c}}
		\toprule
		Method           & Backbone        & $\text{AR}^{100}$ & $\text{AR}^{300}$ & $\text{AR}^{1000}$ \\
		\midrule
		RPN$^*$          & ResNet-50-FPN   & 42.5              & 51.2              & 57.1               \\
		RPN$^*$          & ResNet-101-FPN  & 45.4              & 53.2              & 58.7               \\
		RPN$^*$          & ResNeXt-101-FPN & 47.8              & 55.0              & 59.8               \\
		\midrule
		Libra RPN (ours) & ResNet-50-FPN   & 52.1              & 58.3              & 62.5               \\
		\bottomrule
	\end{tabular}
	\label{tab:rpn}
\end{table}


\begin{table}[htb]
	\centering
	\caption{Ablation studies of IoU-balanced sampling on COCO \emph{val-2017}.}
	\vspace{-5pt}
	\addtolength{\tabcolsep}{-2pt}
	\begin{tabular}{*{12}{c}}
		\toprule
		Settings    & AP   & $\text{AP}_{50}$ & $\text{AP}_{75}$ & $\text{AP}_{S}$ & $\text{AP}_{M}$ & $\text{AP}_{L}$ \\
		\midrule
		Baseline    & 35.9 & 58.0             & 38.4             & 21.2            & 39.5            & 46.4            \\
		\midrule
		Pos Balance & 36.1 & 58.2             & 38.2             & 21.3            & 40.2            & 47.3            \\
		$K = 2$     & 36.7 & 57.8             & 39.9             & 20.5            & 39.9            & 48.9            \\
		$K = 3$     & 36.8 & 57.9             & 39.8             & 21.4            & 39.9            & 48.7            \\
		$K = 5$     & 36.7 & 57.7             & 39.9             & 19.9            & 40.1            & 48.7            \\
		\bottomrule
	\end{tabular}
	\vspace{-10pt}
	\label{tab:iou}
\end{table}

% Main Results
\subsection{Main Results}
We compare Libra R-CNN with the state-of-the-art object detection approaches on the COCO \emph{test-dev} in Tabel~\ref{tab:overall-results}.
For fair comparisons with corresponding baselines, we report our re-implemented results of them, which are generally higher than that were reported in papers.
Through the overall balanced design, Libra R-CNN achieves 38.7 AP with ResNet-50~\cite{resnet}, which is $2.5$ points higher AP than FPN Faster R-CNN.
With ResNeXt-101-64x4d~\cite{resnext}, a much more powerful feature extractor, Libra R-CNN achieves 43.0 AP.

Apart from the two-stage frameworks, we further extend our Libra R-CNN to single stage detectors and report the results of Libra RetinaNet.
Considering that there is no sampling procedure in RetinaNet~\cite{focalloss}, Libra RetinaNet only integrates balanced feature pyramid and balanced L1 loss.
Without bells and whistles, Libra RetinaNet brings 2.0 points higher AP with ResNet-50 and achieves 37.8 AP.


Our method can also enhance the average recall of proposal generation.
As shown in Table~\ref{tab:rpn}, Libra RPN brings $9.2$ points higher AR$^{100}$, $6.9$ points higher AR$^{300}$ and $5.4$ points higher AR$^{1000}$ compared with RPN with ResNet-50 respectively.
Note that larger backbones only bring little gain to RPN.
Libra RPN can achieve 4.3 points higher AR$^{100}$ than ResNeXt-101-64x4d only with a ResNet-50 backbone.
The significant improvements from Libra RPN validate that the potential of RPN is much more exploited with the effective balanced training.

% \vspace{-5pt}
\subsection{Ablation Experiments}
% \vspace{-5pt}
\paragraph{Overall Ablation Studies.}
To analyze the importance of each proposed component, we report the overall ablation studies in Table~\ref{tab:overall-ablation}.
We gradually add IoU-balanced sampling, balanced feature pyramid and balanced L1 loss on ResNet-50 FPN Faster R-CNN baseline.
Experiments for ablation studies are implemented with the same pre-computed proposals for fair comparisons.

\vspace{-12pt}
\paragraph{1) IoU-balanced Sampling.}
IoU-balanced sampling brings 0.9 points higher box AP than the ResNet-50 FPN Faster R-CNN baseline, validating the effectiveness of this cheap hard mining method.
We also visualize the training samples under random sampling and IoU-balanced sampling in Figure~\ref{fig:sampling}.
It can be seen that the selected samples are gathered to the regions where we are more interested in instead of randomly appearing around the target.

\vspace{-12pt}
\paragraph{2) Balanced Feature Pyramid.}
Balanced feature pyramid improves the box AP from 36.8 to 37.7.
Results in small, medium and large scales are consistently improved, which validate that the balanced semantic features balanced low-level and high-level information in each level and yield consistent improvements.


\vspace{-12pt}
\paragraph{3) Balanced L1 Loss.}
Balanced L1 loss improves the box AP from 37.7 to 38.5.
To be more specific, most of the improvements are from $AP_{75}$, which yields 1.1 points higher AP compared with corresponding baseline.
This result validates that the localization accuracy is much improved.

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{figures/sampling_view}
	\caption{Visualization of training samples under random sampling and IoU-balanced sampling respectively.}
	\label{fig:sampling}
	\vspace{-10pt}
\end{figure}

\vspace{-12pt}
\paragraph{Ablation Studies on IoU-balanced Sampling.}
Experimental results with different implementations of IoU-balanced sampling are shown in Table~\ref{tab:iou}.
We first verify the effectiveness of the complementary part, \ie sampling equal number of positive samples for each ground truth, which is stated in Section~\ref{subsec:sample} and denoted by \emph{Pos Balance} in Table~\ref{tab:iou}.
Since there are too little positive samples to explore the potential of this method, this sampling method provides only small improvements (0.2 points higher AP) compared to ResNet-50 FPN Faster R-CNN baseline.

Then we evaluate the effectiveness of IoU-balanced sampling for negative samples with different hyper-parameters $K$, which denotes the number of intervals.
Experiments in Table~\ref{tab:iou} show that the results are very close to each other when the parameter $K$ is set as 2, 3 or 5.
Therefore, the number of sampling interval is not much crucial in our IoU-balanced sampling, as long as the hard negatives are more likely selected.

\vspace{-12pt}
\paragraph{Ablation Studies on Balanced Feature Pyramid.}
Ablation studies of balanced feature pyramid are shown in Table~\ref{tab:fpn}.
We also report the experiments with PAFPN~\cite{panet}.
We first implement balanced feature pyramid only with integration.
Results show that the naive feature integration brings 0.4 points higher box AP than the corresponding baseline.
Note there is no refinement and no parameter added in this procedure.
With this simple method, each resolution obtains equal information from others.
Although this result is comparable with the one of PAFPN~\cite{panet}, we reach the feature level balance without extra convolutions, validating the effectiveness of this simple method.

Along with the embedded Gaussian non-local attention~\cite{nonlocal}, balanced feature pyramid can be further enhanced and improve the final results.
Our balanced feature pyramid is able to achieve 36.8 AP on COCO dataset, which is 0.9 points higher AP than ResNet-50 FPN Faster R-CNN baseline.
More importantly, the balanced semantic features have no conflict with PAFPN.
Based on the PAFPN, we include our feature balancing scheme and denote this implementation by Balanced PAFPN in Table~\ref{tab:fpn}.
Results show that the Balanced PAFPN is able to achieve 37.2 box AP on COCO dataset, with 0.9 points higher AP compared with the PAFPN.

% identity pyramid
\begin{table}[t]
	\centering
	\caption{Ablation studies of balanced semantic pyramid on COCO \emph{val-2017}.}
	\vspace{-5pt}
	\addtolength{\tabcolsep}{-2pt}
	\begin{tabular}{*{12}{c}}
		\toprule
		Settings          & AP   & $\text{AP}_{50}$ & $\text{AP}_{75}$ & $\text{AP}_{S}$ & $\text{AP}_{M}$ & $\text{AP}_{L}$ \\
		\midrule
		Baseline          & 35.9 & 58.0             & 38.4             & 21.2            & 39.5            & 46.4            \\
		\midrule
		Integration       & 36.3 & 58.8             & 38.8             & 21.2            & 40.1            & 46.3            \\
		Refinement        & 36.8 & 59.5             & 39.5             & 22.3            & 40.6            & 46.5            \\
		\midrule
		PAFPN\cite{panet} & 36.3 & 58.4             & 39.0             & 21.7            & 39.9            & 46.3            \\
		Balanced PAFPN    & 37.2 & 60.0             & 39.8             & 22.7            & 40.8            & 47.4            \\
		\bottomrule
	\end{tabular}
	\vspace{-10pt}
	\label{tab:fpn}
\end{table}


% balanced l1 loss
\begin{table}[htb]
	\centering
	\caption{Ablation studies of balanced L1 loss on COCO \emph{val-2017}. The numbers in the parentheses indicate the loss weight.}
	\vspace{-5pt}
	\addtolength{\tabcolsep}{-2pt}
	\begin{tabular}{*{12}{c}}
		\toprule
		Settings                        & AP   & $\text{AP}_{50}$ & $\text{AP}_{75}$ & $\text{AP}_{S}$ & $\text{AP}_{M}$ & $\text{AP}_{L}$ \\
		\midrule
		Baseline                        & 35.9 & 58.0             & 38.4             & 21.2            & 39.5            & 46.4            \\
		\midrule
		loss weight = 1.5               & 36.4 & 58.0             & 39.7             & 20.8            & 39.9            & 47.5            \\
		loss weight = 2.0               & 36.2 & 57.3             & 39.5             & 20.2            & 40.0            & 47.5            \\
		L1 Loss (1.0)                   & 36.4 & 57.4 & 39.1 & 21.0 & 39.7 &  47.9\\
		L1 Loss (1.5)                   & 36.6 & 57.2 & 39.8 & 20.2 & 40.0 & 48.2 \\
		L1 Loss (2.0)                   & 36.4 & 56.5 & 39.6 & 20.1 & 39.8 & 48.2 \\
		\midrule
		$\alpha = 0.2$,  $\gamma = 1.0$ & 36.7 & 58.1             & 39.5             & 21.4            & 40.4            & 47.4            \\
		$\alpha = 0.3$,  $\gamma = 1.0$ & 36.5 & 58.2             & 39.2             & 21.6            & 40.2            & 47.2            \\
		$\alpha = 0.5$,  $\gamma = 1.0$ & 36.5 & 58.2             & 39.2             & 21.5            & 39.9            & 47.2            \\
		\midrule
		$\alpha = 0.5$,  $\gamma = 1.5$ & 37.2 & 58.0             & 40.0             & 21.3            & 40.9            & 47.9            \\
		$\alpha = 0.5$,  $\gamma = 2.0$ & 37.0 & 58.0             & 40.0             & 21.2            & 40.8            & 47.6            \\
		\bottomrule
	\end{tabular}
	\vspace{-10pt}
	\label{tab:loss}
\end{table}



\vspace{-12pt}
\paragraph{Ablation Studies on Balanced L1 Loss.}
Ablation studies of balanced L1 loss are shown in Table~\ref{tab:loss}.
We observe that the localization loss is mostly half of the recognition loss.
Therefore, we first verify the performance when raising loss weight directly.
Results show that tuning loss weight only improves the result by 0.5 points.
And the result with a loss weight of 2.0 starts to drop down.
These results show that the outliers bring negative influence on the training process, and leave the potential of model architecture from being fully exploited.
We also conduct experiments with L1 loss for comparisons.
Experiments show that the results are inferior to ours.
Although the overall results are improved, the AP$_{50}$ and AP$_S$ drop obviously.

In order to compare with tuning loss weight directly, we first validate the effectiveness of balanced L1 loss when $\gamma = 1$.
Balanced L1 loss is able to bring 0.8 points higher AP than baseline.
With our best setting, balanced L1 loss finally achieves $37.2$ AP, which is 1.3 points higher than the ResNet-50 FPN Faster R-CNN baseline.
These experimental results validate that our balanced L1 achieves a more balanced training and makes the model better converged.
