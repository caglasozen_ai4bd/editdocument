% !TEX root = ../1128.tex

\section{Introduction}

%-----Object detection background-----
Along with the advances in deep convolutional networks, recent years have
seen remarkable progress in object detection.
A number of detection frameworks such as
Faster R-CNN~\cite{frcnn},
RetinaNet~\cite{focalloss},
and Cascaded R-CNN~\cite{cascadercnn} have been developed,
which have substantially pushed forward the state of the art.
%
Despite the apparent differences in the pipeline architectures,
\eg~single-stage \vs two-stage, modern detection frameworks mostly follow
a common training paradigm, namely, sampling regions, extracting features therefrom,
and then jointly recognizing the categories and refining the locations
under the guidance of a standard multi-task objective function.

\begin{figure}
	\setlength{\belowcaptionskip}{-15pt}
	\centering
	\includegraphics[width=\linewidth]{figures/imbalance}
	\caption{Imbalance consists in (a) sample level (b) feature level and (c) objective level,
	which prevents the well-designed model architectures from being fully exploited.}
	\label{fig:imbalance}
\end{figure}

%------Imbalance phenomenon----------
Based on this paradigm, the success of the object detector training depends on
three key aspects:
(1) whether the selected region samples are representative,
(2) whether the extracted visual features are fully utilized, and
(3) whether the designed objective function is optimal.
%
However, our study reveals that the typical training process is significantly imbalanced in all these aspects.
This imbalance issue prevents the power of well-designed model architectures from being fully exploited,
thus limiting the overall performance, which is shown in Figure~\ref{fig:imbalance}.
%
Below, we describe these issues in turn:

\vspace{-15pt}
\paragraph{\emph{1) Sample level imbalance:}}
When training an object detector, hard samples are particularly valuable
as they are more effective to improve the detection performance.
However, the random sampling scheme usually results in
the selected samples dominated by easy ones.
The popularized hard mining methods, \eg~OHEM~\cite{ohem}, can help driving
the focus towards hard samples.
However, they are often sensitive to noise labels and incurring considerable memory and computing costs.
Focal loss~\cite{focalloss} also alleviates this problem in single-stage detectors,
but is found little improvement when extended to R-CNN as the majority easy negatives are filtered by the two-stage procedure.
Hence, this issue needs to be solved more elegantly.

\vspace{-15pt}
\paragraph{\emph{2) Feature level imbalance:}}
Deep high-level features in backbones are with more semantic meanings while the shallow low-level features are more content descriptive~\cite{zeiler2014visualizing}.
Recently, feature integration via lateral connections in FPN~\cite{fpn} and PANet~\cite{panet} have advanced the development of object detection.
These methods inspire us that the low-level and high-level information are complementary for object detection.
The approach that how them are utilized to integrate the pyramidal representations determines the detection performance.
However, what is the best approach to integrate them together?
Our study reveals that the integrated features should possess balanced information from each resolution.
But the sequential manner in aforementioned methods will make
integrated features focus more on adjacent resolution but less on others.
The semantic information contained in non-adjacent levels would be diluted once per fusion during the information flow.

\vspace{-15pt}
\paragraph{\emph{3) Objective level imbalance:}}
%
A detector needs to carry out two tasks, \ie~classification and localization.
Thus two different goals are incorporated in the training objective.
If they are not properly balanced, one goal may be compromised, leading to suboptimal performance overall~\cite{kendall2017multi}.
The case is the same for the involved samples during the training process.
If they are not properly balanced, the small gradients produced by the easy samples may be drowned into the large gradients produced by the hard ones, thus limiting further refinement.
Hence, we need to rebalance the involved tasks and samples towards the optimal convergence.

%--------Solution-------------

To mitigate the adverse effects caused by these issues,
we propose Libra R-CNN, a simple but effective framework for object detection that
explicitly enforces the balance at all three levels discussed above.
This framework integrates three novel components:
(1) \emph{IoU-balanced sampling}, which mines hard samples according to their IoU with assigned ground-truth.
(2) \emph{balanced feature pyramid},
which strengthens the multi-level features using the same deeply integrated balanced semantic features.
(3) \emph{balanced L1 loss}, which promotes crucial gradients, to rebalance the involved classification, overall localization and accurate localization.

%------Performance-----------
Without bells and whistles, Libra R-CNN
achieves 2.5 points and 2.0 points higher Average Precision (AP) than FPN Faster R-CNN and
RetinaNet respectively on MS COCO~\cite{coco}.
With the 1$\times$ schedule in~\cite{Detectron2018},
Libra R-CNN can obtain 38.7 and 43.0 AP with FPN Faster R-CNN based on ResNet-50
and ResNeXt-101-64x4d respectively.

%-----Main Contribution------
Here, we summarize our main contributions:
(1) We systematically revisit the training process of detectors.
Our study reveals the imbalance problems at three levels that limit the
detection performance.
(2) We propose Libra R-CNN, a framework that rebalances the training
process by combining three new components: IoU-balanced sampling,
balanced feature pyramid, and balanced L1 loss.
(3) We test the proposed framework on MS COCO, consistently obtaining
significant improvements over state-of-the-art detectors, including
both single-stage and two-stage ones.
