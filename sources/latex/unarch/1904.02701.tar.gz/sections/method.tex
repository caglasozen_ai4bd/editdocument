% !TEX root = ../1128.tex
\section{Methodology}

%-------Background and Motivation-------
The overall pipeline of Libra R-CNN is shown in Figure~\ref{fig:overall}.
Our goal is to alleviate the imbalance exists in the training process of detectors using an overall balanced design,
thus exploiting the potential of model architectures as much as possible.
All components will be detailed in the following sections.

\begin{figure*}
	\centering
	\includegraphics[width=\linewidth]{figures/fpn}
	\caption{Pipeline and heatmap visualization of balanced feature pyramid.}
	\label{fig:fpn}
\end{figure*}

%------------IoU-balanced Sampling--------------
\subsection{IoU-balanced Sampling}
\label{subsec:sample}
Let us start with the basic question:
is the overlap between a training sample and its corresponding ground truth associated with its difficulty?
To answer this question, we conduct experiments to find the truth behind.
Results are shown in Figure~\ref{fig:sample_dist}.
We mainly consider hard negative samples, which are known to be the main problem.
We find that more than $60\%$ hard negatives have an overlap greater than $0.05$,
but random sampling only provides us $30\%$ training samples that are greater than the same threshold.
This extreme sample imbalance buries many hard samples into thousands of easy samples.

Motivated by this observation, we propose IoU-balanced sampling: a simple but effective hard mining method without extra cost.
Suppose we need to sample $N$ negative samples from $M$ corresponding candidates.
The selected probability for each sample under random sampling is
\begin{equation}
	\label{equ:random}
	p = \frac{N}{M}.
\end{equation}

To raise the selected probability of hard negatives,
we evenly split the sampling interval into $K$ bins according to IoU.
$N$ demanded negative samples are equally distributed to each bin.
Then we select samples from them uniformly.
Therefore, we get the selected probability under IoU-balanced sampling
\begin{equation}
	\label{equ:sample}
	p_{k} = \frac{N}{K} *\frac{1}{M_{k}}, ~~ k \in [0, K),
\end{equation}
where $M_{k}$ is the number of sampling candidates in the corresponding interval denoted by k.
K is set to 3 by default in our experiments.

The sampled histogram with IoU-balanced sampling is shown by green color in Figure~\ref{fig:sample_dist}.
It can be seen that our IoU-balanced sampling can guide the distribution of training samples close to the one of hard negatives.
Experiments also show that the performance is not sensitive to K,
as long as the samples with higher IoU are more likely selected.

Besides, it is also worth noting that the method is also suitable for hard positive samples.
However, in most cases, there are not enough sampling candidates to extend this procedure into positive samples.
To make the balanced sampling procedure more comprehensive,
we sample equal positive samples for each ground truth as an alternative method.


\begin{figure*}
	\centering
	\includegraphics[width=\linewidth]{figures/loss}
	\caption{We show curves for (a) gradient and (b) loss of our balanced L1 loss here. Smooth L1 loss is also shown in dashed lines. $\gamma$ is set default as 1.0.}
	\label{fig:loss}
	\vspace{-10pt}
\end{figure*}

\subsection{Balanced Feature Pyramid}
Different from former approaches\cite{fpn,panet} that integrate multi-level features using lateral connections,
our key idea is to \emph{strengthen} the multi-level features using the \emph{same} deeply integrated balanced semantic features.
The pipeline is shown in Figure~\ref{fig:fpn}.
It consists of four steps, rescaling, integrating, refining and strengthening.

\vspace{-12pt}
\paragraph{Obtaining balanced semantic features.}
Features at resolution level $l$ are denoted as $C_l$.
The number of multi-level features is denoted as L.
The indexes of involved lowest and highest levels are denoted as $l_{min}$ and $l_{max}$.
In Figure~\ref{fig:fpn}, $C_2$ has the highest resolution.
To integrate multi-level features and preserve their semantic hierarchy at the same time, we first resize the multi-level features $\{C_{2}, C_{3}, C_{4}, C_{5}\}$ to an intermediate size, \ie, the same size as $C_{4}$, with interpolation and max-pooling respectively.
Once the features are rescaled, the \emph{balanced semantic features} are obtained by simple averaging as
\begin{equation}
	\label{equ:IDFeat}
	C = \frac{1}{L}\sum_{l=l_{min}}^{l_{max}}{C_{l}}.
\end{equation}
The obtained features are then rescaled using the same but reverse procedure to strengthen the original features.
Each resolution obtains equal information from others in this procedure.
Note that this procedure does not contain any parameter.
We observe improvement with this nonparametric method, proving the effectiveness of the information flow.

\vspace{-12pt}
\paragraph{Refining balanced semantic features.}
The balanced semantic features can be further refined to be more discriminative.
We found both the refinements with convolutions directly and the non-local module~\cite{nonlocal} work well.
But the non-local module works more stable.
Therefore, we use the embedded Gaussian non-local attention as default in this paper.
The refining step helps us enhance the integrated features and further improve the results.

With this method, features from low-level to high-level are aggregated at the same time.
The outputs $\{P_{2}, P_{3}, P_{4}, P_{5}\}$ are used for object detection following the same pipeline in FPN.
It is also worth mentioning that our balanced feature pyramid can work as complementary with recent solutions such as FPN and PAFPN without any conflict.

\begin{table*}[htb]
	\centering
	\caption{Comparisons with state-of-the-art methods on COCO \emph{test-dev}.
	  The symbol ``*'' means our re-implemented results.
	  The ``$1\times$'', ``$2\times$'' training schedules follow the settings explained in Detectron~\cite{Detectron2018}.}
	\vspace{-5pt}
	\addtolength{\tabcolsep}{1pt}
	\begin{tabular}{*{12}{c}}
		\toprule
		Method                       & Backbone  & Schedule   &  AP & $\text{AP}_{50}$ & $\text{AP}_{75}$ & $\text{AP}_{S}$ & $\text{AP}_{M}$ & $\text{AP}_{L}$  \\
		\midrule
		YOLOv2~\cite{yolo9000} & DarkNet-19 & - & 21.6 & 44.0 & 19.2 & 5.0 & 22.4 & 35.5\\
		SSD512~\cite{ssd} & ResNet-101 & - & 31.2 & 50.4 & 33.3 & 10.2 & 34.5 & 49.8 \\
		RetinaNet~\cite{focalloss} & ResNet-101-FPN & -  & 39.1 & 59.1 & 42.3 & 21.8 & 42.7 & 50.2 \\
		Faster R-CNN~\cite{fpn} & ResNet-101-FPN & - & 36.2 & 59.1 & 39.0 & 18.2 & 39.0 & 48.2 \\
		Deformable R-FCN~\cite{rfcn} & Inception-ResNet-v2 & -  & 37.5 & 58.0 & 40.8 & 19.4 & 40.1 & 52.5 \\
		Mask R-CNN~\cite{maskrcnn} & ResNet-101-FPN & - & 38.2 & 60.3 & 41.7 & 20.1 & 41.1 & 50.2 \\
		\midrule
		Faster R-CNN$^*$ & ResNet-50-FPN & $1\times$ & 36.2 & 58.5 & 38.9 & 21.0 & 38.9 & 45.3            \\
		Faster R-CNN$^*$ & ResNet-101-FPN & $1\times$ & 38.8 & 60.9 & 42.1 & 22.6 & 42.4 & 48.5         \\
		Faster R-CNN$^*$ & ResNet-101-FPN & $2\times$ & 39.7 & 61.3 & 43.4 & 22.1 & 43.1 & 50.3           \\
		Faster R-CNN$^*$ & ResNeXt-101-FPN & $1\times$ & 41.9 & 63.9 & 45.9 & 25.0 & 45.3 & 52.3 \\
		RetinaNet$^*$ & ResNet-50-FPN & $1\times$ & 35.8 & 55.3 & 38.6 & 20.0 & 39.0 & 45.1  \\
		\midrule
		Libra R-CNN (ours) & ResNet-50-FPN & $1\times$ & 38.7 & 59.9 & 42.0 & 22.5 & 41.1 & 48.7 \\
		Libra R-CNN (ours) & ResNet-101-FPN & $1\times$ & 40.3 & 61.3 & 43.9 & 22.9 & 43.1 & 51.0 \\
		Libra R-CNN (ours) & ResNet-101-FPN & $2\times$ & 41.1 & 62.1 & 44.7 & 23.4 & 43.7 & 52.5 \\
		Libra R-CNN (ours) & ResNeXt-101-FPN  & $1\times$ & 43.0 & 64.0 & 47.0 & 25.3 & 45.6 & 54.6 \\
		Libra RetinaNet (ours) & ResNet-50-FPN & $1\times$ & 37.8 & 56.9 & 40.5 & 21.2 & 40.9 & 47.7  \\
		\bottomrule
	\end{tabular}
	\vspace{-5pt}
	\label{tab:overall-results}
\end{table*}

% ------Balanced L1 Loss------
\subsection{Balanced L1 Loss}
Classification and localization problems are solved simultaneously under the guidance of a multi-task loss since Fast R-CNN~\cite{fastrcnn}, which is defined as
\begin{equation}
	\label{equ:multi-task}
	L_{p, u, t^u, v} = L_{cls}(p, u) + \lambda[u \ge 1]L_{loc}(t^u, v).
\end{equation}
$L_{cls}$ and $L_{loc}$ are objective functions corresponding to recognition and localization respectively.
Predictions and targets in $L_{cls}$ are denoted as $p$ and $u$.
$t^u$ is the corresponding regression results with class $u$.
$v$ is the regression target.
$\lambda$ is used for tuning the loss weight under multi-task learning.
We call samples with a loss greater than or equal to 1.0 outliers.
The other samples are called inliers.

A natural solution for balancing the involved tasks is to tune the loss weights of them.
However, owing to the unbounded regression targets,
directly raising the weight of localization loss will make the model more sensitive to outliers.
These outliers, which can be regarded as hard samples, will produce excessively large gradients that are harmful to the training process.
The inliers, which can be regarded as the easy samples, contribute little gradient to the overall gradients compared with the outliers.
To be more specific, inliers only contribute 30\% gradients average per sample compared with outliers.
Considering these issues, we propose balanced L1 loss, which is denoted as $L_{b}$.

Balanced L1 loss is derived from the conventional smooth L1 loss,
in which an inflection point is set to separate inliers from outliners, and clip the large gradients produced by outliers with a maximum value of 1.0, as shown by the dashed lines in Figure~\ref{fig:loss}-(a).
The key idea of balanced L1 loss is promoting the crucial regression gradients,
\ie gradients from inliers (accurate samples), to rebalance the involved samples and tasks, thus achieving a more balanced training within classification, overall localization and accurate localization.
Localization loss $L_{loc}$ uses balanced L1 loss is defined as
\begin{equation}
	\label{equ:loc}
	L_{loc} = \sum_{i\in\{x,y,w,h\}}{L_{b}(t_{i}^{u} - v_i)},
\end{equation}
and its corresponding formulation of gradients follows
\begin{equation}
	\label{equ:partial}
	\frac{\partial{L_{loc}}}{\partial{w}} \varpropto
	\frac{\partial{L_{b}}}{\partial{t_i^u}} \varpropto
	\frac{\partial{L_{b}}}{\partial{x}},
\end{equation}
Based on the formulation above, we design a promoted gradient formulation as
\begin{equation}
	\label{equ:gradient}
	\frac{\partial{L_{b}}}{\partial{x}} =
	\begin{cases}
		\alpha ln(b|x| + 1) & \text{if $|x| < 1$} \\
		\gamma  & \text{otherwise},
	\end{cases}
\end{equation}

Figure~\ref{fig:loss}-(a) shows that our balanced L1 loss increases the gradients of inliers under the control of a factor denoted as $\alpha$.
A small $\alpha$ increases more gradient for inliers, but the gradients of outliers are not influenced.
Besides, an overall promotion magnification controlled by $\gamma$ is also brought in for tuning the upper bound of regression errors, which can help the objective function better balancing involved tasks.
The two factors that control different aspects are mutually enhanced to reach a more balanced training.
$b$ is used to ensure $L_{b}(x=1)$ has the same value for both formulations in Eq. (\ref{equ:loss}).

By integrating the gradient formulation above, we can get the balanced L1 loss
\begin{equation}
	\label{equ:loss}
	L_{b}(x) =
	\begin{cases}
		\frac{\alpha}{b}(b|x| + 1)ln(b|x| + 1) - \alpha |x|  & \text{if $|x| < 1$}\\
		\gamma|x| + C  & \text{otherwise},
	\end{cases}
\end{equation}
in which the parameters $\gamma$, $\alpha$, and $b$ are constrained by
\begin{equation}
	\alpha ln(b+1) = \gamma.
\end{equation}
The default parameters are set as $\alpha = 0.5$ and $\gamma = 1.5$ in our experiments.
