\section{Introduction}

% Here we have the typical use of a "T" for an initial drop letter
% and "HIS" in caps to complete the first word.
\IEEEPARstart{I}{n} the past decade, overhead image data collection rates have expanded to the petabyte scale \cite{high_data_volume, moody2016building, xia2017exploiting}. This imagery is used for a wide variety of applications including disaster relief \cite{application_disaster_relief}, environmental monitoring \cite{application_environmental}, and Intelligence, Surveillance, and Reconnaissance (ISR) \cite{application_isr}. Given the scale of this data and the need for fast processing, human analysis alone is no longer a tractable solution to these problems. 

Correspondingly, algorithmic processing for this imagery has improved rapidly, with a recent emphasis on the use of convolutional neural networks (CNNs). CNNs, originally proved significant by Yann LeCun in the late 1990's for solving simple image recognition problems \cite{lecun1999object}, rose to prominence in 2012 after a breakthrough many-layer (deep) architecture was introduced by Alex Krizhevsky \cite{krizhevsky2012imagenet}. The use of learned feature representations from CNNs quickly replaced a host of hand-crafted image features, including Histogram of Oriented Gradients (HOG) \cite{dalal2005histograms}, Local Binary Patterns (LBP) \cite{ojala2002multiresolution}, and Haar-like features \cite{viola2001rapid}.

In nearly every major visual recognition contest since 2012, all top solutions have used CNNs \cite{ILSVRC15, minetto2018hydra, zeiler2014visualizing}. Likewise, the top performing solutions on nearly all major visual recognition datasets employ CNNs\footnote{Best results on standard datasets:\\\url{http://rodrigob.github.io/are_we_there_yet/build/}} \cite{wan2013regularization, springenberg2014striving, snoek2015scalable, yang2015deep, lee2016generalizing}. A variety of robust neural network software frameworks have been developed in recent years to capitalize on the need for high-level network architecture design and deployment capability. These frameworks, including TensorFlow, Caffe, and PyTorch have enabled a large and active community working towards the development of neural networks for visual recognition.

Given the importance of visual recognition in overhead imagery, the criticality of using machines for this task, and the dominance of CNNs as a vehicle for doing so, it follows that we want to acquire imagery which is well-suited to visual recognition with CNNs. Consequently, the sensing systems which acquire this imagery must be designed to gather imagery optimal for visual recognition with CNNs. 

Broadly, we pose the following problem:
given a dataset of images $\mathcal{D}$ collected from a sensor with parameters $P$, find the set of values for $p \in P$ which optimize the performance of some image recognition model $m \in M$ on dataset $\mathcal{D}$ according to an objective function $L$ of desired metrics. Note that this $L$ would include the monetary cost of the sensor in a real-world setting. The problem to be solved e.g., classification, detection, or retrieval, is treated as a part of the model.
\begin{equation}
\label{principle_equation}
\argmin_{p \in P}{L(P; m, \mathcal{D})}
\end{equation}

This work describes how this optimization problem can be constructed using recent image data and state-of-the-art visual recognition methods, and we present results for several configurations of the problem. Importantly, the models $m \in M$ that we choose are CNN variants and image quality algorithms, but we emphasize that the method is general and can be applied for all $m \in M$, including the Human Visual System (HVS). We compare metrics of human recognition with metrics of machine recognition, and demonstrate an important pattern in the relationship between these metrics and the observed sensor parameters.

A diagram of our approach is presented in Figure \ref{fig:system}. The variables and parameters of Equation \ref{principle_equation} correspond directly to the elements of this diagram.

\begin{figure*}[!t]
\centering
\includegraphics[width=\textwidth,draft=false]{system_diagram.pdf} 
\caption{A diagram of our approach. We take existing imagery captured from some image system, then regenerate that imagery as though it were captured by an image system with parameters of our choosing. This regenerated imagery is partitioned into standard training and testing sets, then used to train a CNN classifier model. Results on the testing imagery are collected, and the process is repeated for many different parameter configurations in order to observe relationships between imager parameters and performance metrics.}
\label{fig:system}
\end{figure*}

In Section \ref{sec:background}, background is provided for relevant concepts in image utility and sensor modeling. In Section \ref{sec:methods}, the sensor simulator and experimental process are described. In Section \ref{sec:results}, the results of several experiments are explained and analyzed. Conclusions are presented in Section \ref{sec:conclusion}.