\section{Background}
\label{sec:background}
\subsection{Image Quality Assessment}
The problem of \textit{image quality assessment} (IQA) is often divided into two categories: \textit{full-reference} IQA, in which an original, undistorted image is available to compare against the target image, and \textit{no-reference} IQA, in which no such original image is available for comparison \cite{wang2004image}. Many algorithms have been developed in each category, which produce a single scalar ``score'' value as output.

Metrics including Mean-Squared Error (MSE) and Peak Signal-to-Noise Ratio (PSNR) are often used for full-reference IQA, commonly to evaluate the effect of a compression algorithm such as JPEG. In  \cite{wang2004image}, Wang et al. argue that these basic metrics do not account for structural similarity perceived by the Human Visual System (HVS). They propose an index for structural similarity, SSIM, which computes a summary of window comparisons between two images, using means and standard deviations of pixels in the windows.

Metrics developed for no-reference IQA include the Blind/Referenceless Image Spatial Quality Evaluator (BRISQUE) \cite{mittal2012no} and the Natural Image Quality Evaluator (NIQE) \cite{mittal2013making}. The BRISQUE algorithm is considered \textit{opinion-aware}, because it is trained on human-annotated image quality evaluations. BRISQUE produces a measure of quality based on ``locally normalized luminance coefficients'', which the authors claim is sufficient to quantify both image naturalness and image distortion. The luminance features are used to train a SVM regressor which produces the final quality score. In contrast, the NIQE algorithm is trained on a corpus of natural images which are \textit{not} annotated for quality or distortion. NIQE computes \textit{natural scene statistic} (NSS) features on this corpus, which are similar to the BRISQUE luminance features. The final NIQE score is computed by comparing a multivariate Gaussian (MVG) fit of the corpus features to the MVG fit of the target image features.

By comparing these IQA metrics with human recognition and CNN recognition, we can assess whether they are similar to either of the two, and if they could be used to model CNN performance for different sensor systems.

\subsection{Image Utility}
We contrast the concept of image quality with \textit{image utility}, which explicitly concerns determining the value of an image for performing some recognition task. The National Imagery Interpretability Rating Scale (NIIRS) was released by the IRARS committee in 1974 for enumerating human interpretability of imagery, usually collected from overhead platforms \cite{leachtenauer1997general}. NIIRS defines a 0-9 rating scale for imagery, with 0 corresponding to complete uninterpretability (due to artifacts, blurring, noise, or occlusion), and 9 corresponding to interpretability of objects at the centimeter scale.

While ``image quality'' is ill-defined, image utility in the sense defined by NIIRS can be measured, tested, and optimized for. Equations have been developed to fit human-annotated NIIRS data, in order to relate common properties of imagery and corresponding collection systems to NIIRS. The General Image Quality Equation (GIQE) is the most well-known of these equations \cite{leachtenauer1997general}. We will discuss version 5 of the GIQE, the most recent, shown in Equation \ref{eq:giqe5}. Values for the constants in this equation are available in \cite{harrington2015general}.

%\small
\begin{dmath}
\label{eq:giqe5}
\text{NIIRS} = A_0 + A_1\log_{10}{\text{GSD}} + A_2 \left[ 1 - \exp{\frac{A_3}{\text{SNR}}} \right] \log_{10}{\text{RER}} + A_4\log_{10}{(\text{RER})^4} + \frac{A_5}{\text{SNR}}
\end{dmath}
%\normalsize

The GIQE5 assumes an analyst will apply their own image enhancements, known as the \textit{softcopy} scenario, and that no generic enhancements are applied prior to viewing \cite{harrington2015general}. We consider the experiments in this work to be analagous to the assumptions of the GIQE5, because no enhancements are applied to the imagery, and the model learns to apply whatever corrections are beneficial to recognition in the imagery.

The GIQE5 has three variables, here explained: Ground Sample Distance (GSD) is the width of a pixel projected from image space onto the ground, given in units of distance per pixel. Signal-to-Noise Ratio (SNR) is the ratio of true signal to noise, mainly consisting of photon noise, dark noise, and read noise. Relative Edge Response (RER) measures sharpness of edges in the imagery \cite{leachtenauer1997general}. 

Of importance to this work are the concepts of \textit{f-number} (FN) and \textit{optical Q}, defined as:
\begin{equation}
%\label{eq:fn}
\text{FN} = \frac{f}{D}
\quad\mathrm{and}\quad 
\text{Q} = \frac{\lambda \text{FN}}{p}
\end{equation}
where $f$ is the \textit{focal length} of the optic, $D$ is the \textit{diameter} of the optic aperture, $p$ is the \textit{pixel pitch} of the detector array, and $\lambda$ is the shortest \textit{wavelength} of light measured. In \cite{fiete1999image}, Fiete shows that optical Q is directly related to image properties associated with quality. For instance, reducing Q increases the SNR and improves image sharpness, but can also lead to aliasing below a certain level.

An important critique of our comparison to GIQE5 is that the equation assumes Q is between 1.0 and 2.0, whereas we mainly study the regime of Q $<$ 1.0. Fiete notes that reducing Q,  ``much below 1.0 can cause objectionable aliasing artifacts'' \cite{fiete1999image}. We leave a study of the Q regime between 1.0 and 2.0 to future work.

NIIRS and the GIQE are useful for tasking of existing imaging systems \cite{irvine1997national}, analyzing capabilities of existing imaging systems \cite{wong2014predicting}, or for the design of new imaging systems \cite{cota2009use}. Consider an image system design scenario: a remote sensing system engineer might select an aperture diameter for the system lens by examining its impact on GSD, SNR, the subsequent NIIRS value, and corresponding tradeoffs with system cost. While increasing optic aperture diameter can give lower GSD and higher SNR, and therefore higher NIIRS, the cost to build and launch the resulting system could create an upper bound for aperture diameter. If we relate this example to Equation \ref{principle_equation}, it can be formulated as:

\begin{equation}
\label{principle_equation_ex1}
\argmin_{D}{L(D; P, \mathcal{D})}
\end{equation}
with
\begin{equation}
L = \alpha \cdot \text{NIIRS}(D; P, \mathcal{D}) + \beta \cdot \text{cost}(D; P)
\end{equation}
where $D$ is the aperture diameter of the system, cost is the monetary cost of the system, NIIRS corresponds to predicted NIIRS for the system, and $\mathcal{D}$ more abstractly corresponds to imagery used to produce a model for NIIRS (possibly the GIQE). The objective function we optimize, $L$, could be a weighted combination of NIIRS and cost, weighted by some parameters $\alpha$ and $\beta$ respectively. All sensing system parameters except $D$ comprise $P$. The objective is parametrized by $P$ and $\mathcal{D}$. Note that in this case, we are optimizing for the HVS by optimizing NIIRS.

While NIIRS and the GIQE have been thoroughly studied, there has been no published evidence to date that they are useful for describing image interpretability for modern computer vision algorithms. More generally, we do not know if human and computer recognition of imagery are similar at all. The experiments detailed in Section \ref{sec:results} aim to explore this question by comparing machine learning metrics with the GIQE for the same system configuration.

Other approaches have alternatively framed the image interpretability problem in terms of \textit{target acquisition performance}. In \cite{vollmerhausen2004targeting}, Vollmerhausen and Jacobs give a detailed history of target acquisition performance, and introduce the Target Task Performance (TTP) metric. This metric, in addition to its predecessor, the Johnson criteria \cite{johnson1985analysis}, are Modulation Transfer Function (MTF)-based metrics \cite{gallimore1991review}. Since NIIRS and the GIQE have been more widely-adopted in practice, we focus our comparison against them.

\subsection{CNNs for Overhead Imagery}
CNNs have rapidly risen to the forefront of algorithms for visual recognition in overhead imagery \cite{zhu2017deep}. Most commonly, CNNs have been used to tackle the classification, retrieval, detection, and segmentation problems. In \cite{chen2016deep, mou2017deep},  different deep CNN architectures are used for image classification in overhead imagery. In \cite{xia2017exploiting}, a systematic investigation of image retrieval with deep learning is conducted for remote sensing imagery. In \cite{long2017accurate}, a CNN-based model is used for object localization (detection) in overhead imagery. In \cite{maggiori2017convolutional}, a fully convolutional architecture is developed for pixelwise classification (semantic segmentation) of satellite imagery.

In addition, large public datasets of overhead imagery are being curated and released at an increasing rate. The SpaceNet dataset, originally released in 2016, comprises hundreds of thousands of polygon labelings for buildings in DigitalGlobe satellite imagery, suited to the segmentation problem \cite{spacenet}. In the xView dataset, one-million objects across 60 classes are annotated with bounding boxes for the detection problem \cite{xview}. In the Functional Map of the World (fMoW) dataset, one-million DigitalGlobe images across 63 classes are annotated with rectangular bounding boxes for the classification problem \cite{fmow}.

This increase in public data volume, combined with innovations in GPU hardware and CNN architectures, have led to a boom in the use of CNNs for overhead imagery. 

\subsection{Sensor Modeling}
A number of tools have been developed for remote sensor modeling, including for image transformation and GIQE computation. LeMaster et al.\cite{lemaster2017pybsm} have developed the ``Python Based Sensor Model'' (pyBSM), which implements the ``ERIM Image Based Sensor Model'' (IBSM) \cite{eismann1996utility}. All GIQE values presented in Section \ref{sec:results} were computed with pyBSM.

Similarly, the Night Vision Integrated Performance Model (NV-IPM) is a software package developed by CERDEC's Night Vision and Electronic Sensors Directorate for modeling image system design\footnote{NV-IPM Web Page:\\\url{https://www.cerdec.army.mil/inside_cerdec/nvesd/integrated_performance_model/}}.

A description of our remote sensor modeling approach is presented in Section \ref{sec:methods}.

\subsection{CV-Based Modeling}
While remote sensor system design is often based on human interpretability through NIIRS or the TTP, there are studies which have measured performance using IQA algorithms, or simple visual recognition algorithms.

In \cite{fanning2012metrics}, Fanning develops a custom MTF-based image generation tool, and uses it to conduct a comparitive study based on the SSIM metric. 

In \cite{lemaster2017pybsm}, LeMaster et al. demonstrate how the face detection problem could be used to impact sensor design by transforming imagery with pyBSM and evaluating performance of a Haar feature-based cascade classifier. Similarly, Howell et al. use the NV-IPM image generation tool for designing a camera to optimize for face recognition with the PITT-PATT algorithm\cite{howell2015face}.

In \cite{zelinski_paper}, a motion detection algorithm is used to evaluate image utility. In \cite{zelinski_thesis}, Zelinski notes the inability of the GIQE to accurately predict NIIRS for sparse aperture systems. He cites this as a reason to instead consider image utility through performance of motion detection and spatial target detection algorithms.

Like in \cite{lemaster2017pybsm}, \cite{yuan2014method} uses a Haar feature-based model for comparing image quality from different sensor designs, but Yuan et al. observe overhead imagery instead of faces. The methods used in that work to simulate imagery from different sensors are less robust than pyBSM, NV-IPM, or our method, involving simple addition of noise, blurring, and image contrast reduction.

We build upon these works by using a large, well-curated overhead image dataset transformed with our own sensor model, and state-of-the-art CNNs instead of traditional hand-crafted feature-based models.