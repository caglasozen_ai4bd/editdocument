\section{Results and Analysis}
\label{sec:results}

\subsection{Calibration}
We begin by training the four target CNN architectures on the original fMoW subset, in order to establish an upper bound for performance. We know this will constitute an upper bound, because the transformations applied to the data are a strict degradation involving downsampling and adding noise. We show results on the validation set in Table \ref{tab:orig_performance}. The peak values for five metrics are listed, in addition to the training epoch at which they were achieved. All four models were pre-trained on ImageNet and were fine-tuned for five epochs. Interestingly, the DenseNet161 architecture performs better than ResNet152, despite ResNet152 performing marginally better on the original ImageNet dataset. This showcases again how different architectures may perform better on different datasets. Therefore, it is important to understand the performance relationship here studied for multiple state-of-the-art architectures, as any could be optimal for a given dataset.

%%% Table with stats on fMoW subset original MS data
\setlength{\heavyrulewidth}{1.5pt}
\setlength{\abovetopsep}{4pt}
\begin{table*}[!htbp]
\centering
\caption{Peak Results for fMoW Subset}
\label{tab:orig_performance}
\begin{tabular}{ccccccccccc}
\toprule
Architecture &  \multicolumn{2}{c}{Top-1 Accuracy} &  \multicolumn{2}{c}{Top-3 Accuracy} &  \multicolumn{2}{c}{AUC} &  \multicolumn{2}{c}{cAP} &  \multicolumn{2}{c}{rAP}\\
\midrule
{} & Epoch & Value & Epoch & Value & Epoch & Value & Epoch & Value & Epoch & Value\\
SqueezeNet1.1 & 13 & 0.476 & 13 & 0.692 & 13 & 0.901 & 13 & 0.391 & 13 & 0.151\\
VGG16 & 3 & 0.579 & 3 & 0.771 & 3 & 0.926 & 5 & 0.518 & 3 & 0.312\\
ResNet152 & 3 & 0.602 & 3 & 0.792 & 3 & 0.947 & 5 & 0.621 & 5 & 0.376\\
DenseNet161 & 3 & 0.623 & 2 & 0.809 & 3 & 0.952 & 4 & 0.650 & 4 & 0.405\\
\bottomrule
\end{tabular}
\end{table*}

\subsection{Baseline}
For our baseline experiment, we consider the relationship between rAP of DenseNet161 and optic focal length. Images were pre-processed using the \textit{crop} method, and the model was trained for five epochs. This result is compared with NIIRS as computed with the GIQE5. The result is shown in Figure \ref{fig:cornerstone}. We choose to compare rAP in most experiments, because it can be generalized to unknown classes, as shown in Section \ref{subsec:generalize}. We refer to the baseline experiment in the following experiment subsections, in most cases with just one parameter altered from Table \ref{tab:experiment_parameters}.

If we return to the original formulation from Equation \ref{principle_equation}, we can consider the baseline problem as:
\begin{equation}
\argmax_{f}{\text{rAP}(f; P, m, \mathcal{D})}
\label{principle_equation_ex2}
\end{equation}
where $m$ is DenseNet161, $\mathcal{D}$ is the fMoW subset, and $P$ corresponds to all image system parameters except for $f$, the focal length. 

We must take care to discern the performance metrics exhibited here from NIIRS. While these experiments measure how well a classifier performs at recognizing specific objects, NIIRS measures how well all objects can be interpreted at each tier of its scale. For this reason, we focus on comparing optima between the two, instead of attempting to relate absolute performance. In this case, we compare:
\begin{equation}
\argmax_{f}{\text{rAP}} \;\; \text{vs.} \; \argmax_{f}{\text{NIIRS}}
\label{principle_equation_comparison}
\end{equation}

% PLOT standard relationship
\begin{figure}[!t]
\centering
\includegraphics[trim={0 0 3cm 0},width=0.5\textwidth,draft=false]{sepsense_arxiv.pdf} 
\caption{This plot visualizes the baseline experiment of this work, in which rAP is averaged across validation partitions of the fMoW subset, for a range of sensor system focal length values ($D$=0.05m). An example image of a race track is shown for different focal length values to demonstrate the visual changes which result from the transformation. A clear peak is present for rAP, at which point the optimal tradeoff between resolution and noise for the CNN has been reached. The GIQE5 NIIRS curve for these focal length values has a different shape, peaking at a larger focal length value. This shows that CNN performance and human interpretability may not have the same relationship with sensor system parameters. Note that rAP has been normalized to [0, 1], while GIQE5 NIIRS is unaltered, to better compare the peaks between the two curves.}
\label{fig:cornerstone}
\end{figure}

We show the performance relationship for the baseline experiment for all considered problems and metrics in Figure \ref{fig:multi_stat}. Notice that the two accuracy metrics peak at $f=0.4$, the two AP metrics peak at $f=0.5$, and the AUC metric peaks at $f=0.6$. Since all five metrics are near their peak from $f \in [0.4, 0.6]$, focal length may be selected based on another consideration within this range, such as cost.

\begin{figure}[!t]
\centering
\includegraphics[trim={0.25cm 0 0 0},width=0.49\textwidth]{multi_stat.pdf} 
\caption{Multiple metrics are shown  for validation set performance of the baseline model as a function of varied focal length. Notice that the normalized curves are nearly identical, but peak at different focal length values. The importance of this variation will depend on precision and cost requirements for a given application.}
\label{fig:multi_stat}
\end{figure}

We give a breakdown of rAP performance for ten individual classes in Figure \ref{fig:per_class_plots}. While the relationship between focal length and rAP for each class is similar, there is significant variation in the focal length value at which rAP is greatest, e.g., 0.3 for swimming pool vs. 0.8 for solar farm. This could be explained by reliance on spectral vs. spatial features. At lower focal length values, spectral features are better preserved than spatial features, explaining why the bright blue of the swimming pool would be more easily recognized. At higher focal length values, as less light enters the system and more noise is introduced, spatial features have greater weight than spectral features, explaining why the gridded pattern of the solar farm would be more easily recognized. In addition, some classes have a greater range in rAP with respect to focal length, e.g., 0.4 for road bridge vs. 0.2 for crop field. These observations indicate that the spectral and spatial features of the imagery in these classes, independent of the overall quality of the imagery, have a variable response with respect to changes in resolution and noise.
\begin{figure*}[!t]
\centering
\includegraphics[width=\textwidth]{per_class_plots_arxiv.pdf} 
\caption{Plots of rAP are shown for 10 different classes for the baseline experiment, as opposed to averages used for the other experiments. The black dotted line in each plot indicates a lower bound for rAP, if the image embeddings were randomly distributed.}
\label{fig:per_class_plots}
\end{figure*}

\subsection{Varying Aperture Diameter}
\label{subsec:diam}
% Since this the peak point corresponds to peak angular radiance, this result is intuitive.
We here explore variation of optic aperture diameter, in addition to focal length. First, we consider how a model pre-trained on ImageNet, but without fine-tuning on the fMoW subset, will perform as aperture diameter changes. The results of this experiment are shown in Figure \ref{fig:vary_diameter_epoch0}. As mentioned in Section \ref{subsec:aftersim}, this was the only scenario in which standard score normalization (ImageNet mean, standard deviation) was used. 

% PLOT 0 epochs for rAP vary_diameter_epoch0.pdf
\begin{figure*}[!t]
\centering
\includegraphics[width=\textwidth]{vary_diameter_norm0.pdf} 
\caption{Baseline system evaluated directly from pre-trained weights, showing changing rAP as a function of focal length and Q for six aperture diameter values. The solid lines correspond to experimental results, and the dotted lines to GIQE5 NIIRS. Notice the similarity to the baseline result, despite the network not having seen any overhead images during training.}
\label{fig:vary_diameter_epoch0}
\end{figure*}

Then, we consider how the pre-trained model will perform once it has been fine-tuned for five epochs, just as in the baseline. The results of this experiment are shown in Figure \ref{fig:vary_diameter_niirs}. First, notice that the relationship between the systems with different aperture diameter is similar for low focal length, but diverges as focal length increases. This can be understood through Figure \ref{fig:d_compare}, in which the quantity of noise present in the low-aperture diameter system becomes significantly greater as focal length increases.

Next, notice that the pre-trained model without fine-tuning better corresponds to GIQE5 NIIRS than the fine-tuned model. While the axes have been adjusted to better compare against NIIRS, notice that the rAP values on the left y-axis of Figure \ref{fig:vary_diameter_niirs} are much greater than in Figure \ref{fig:vary_diameter_epoch0}. Therefore, while the alignment between NIIRS and rAP in \ref{fig:vary_diameter_epoch0} may appear significant, a designer would more likely want to optimize for the relationship exhibited in  \ref{fig:vary_diameter_niirs}, in which the model performance is greater.

Still, this result raises an interesting question: could it be the case that the CNN pre-trained on ImageNet has performance more similar to the GIQE because it more closely models human recognition? In this case, improved performance when fine-tuned on the satellite imagery could suggest that when the network ``specialized'' towards a specific domain, it not only gained improved performance over a generic recognizer, but attained an altered capability with respect to tolerance of artifacts. From an evolutionary perspective, it is unreasonable to assume that the human visual system is perfectly attuned to this domain of imagery, and to optimally extract patterns when some combination of noise and blurring are present. Further, a computational solution may not be not phased by artifacts such as aliasing, which look displeasing to humans, but do not significantly alter the semantic information present.

% PLOT vs. FL and vs. FN (or Q)
\begin{figure*}[!t]
\centering
\includegraphics[width=\textwidth]{vary_diameter_q_niirs.pdf} 
\caption{Baseline system fine-tuned from pre-trained weights, showing changing rAP as a function of focal length and Q for six aperture diameter values. The solid lines correspond to experimental results, and the dotted lines to GIQE5 NIIRS. Notice that the peak rAP values, denoted by triangles, are closely aligned in the plot vs. Q on the right, unlike in the plot vs. focal length on the left. This shows that Q is significant in describing image utility.}
\label{fig:vary_diameter_niirs}
\end{figure*}

\subsection{Varying Architectures}
As observed in comparing Tables \ref{tab:arch_info} and \ref{tab:orig_performance}, different architectures may be optimal for different datasets and problems. Therefore, we consider it important to compare varying architectures, to test if the same parameter-performance relationship manifests. 

In Figure \ref{fig:vary_arch}, the three architectures described in Section \ref{subsec:cnn} are compared for the baseline experiment. All three models attained peak performance at the same focal length value, suggesting that the models reacted similarly to variation in resolution and noise, even while they achieved different levels of absolute performance. This result indicates a consistency with respect to visual recognition with CNNs. This consistency, combined with the difference between CNN performance and GIQE5 NIIRS shown in Figure \ref{fig:vary_diameter_niirs}, could suggest that CNNs in general are differently affected by resolution and noise than the HVS.

% PLOT densenet vs. squeezenet
\begin{figure}[!t]
\centering
\includegraphics[trim={0.25cm 0 0 0},width=0.49\textwidth,draft=false]{vary_arch.pdf} 
\caption{SqueezeNet1.1, VGG16, ResNet152, and DenseNet161 are compared for the baseline experiment. Notice that while SqueezeNet1.1 has a different optimal focal length of 0.6m vs. 0.5m for the other architectures, the normalized curves are very similar.}
\label{fig:vary_arch}
\end{figure}

\subsection{Varying Learning Objective}
In this experiment, we compare the performance of the baseline scenario with cross-entropy loss training vs. triplet margin loss training. The goal is to test whether the same focal length-rAP relationship manifests when optimizing for two significantly different objectives. As described in Section \ref{subsec:mod_mod}, the two variations of DenseNet161 used differ in the last two layers. The results shown in Figure \ref{fig:vary_triplet_arch} indicate that the focal length-rAP relationship is consistent between the two objectives. 

% Show comparison of architectures for triplet learning
\begin{figure}[!t]
\centering
\includegraphics[trim={0.25cm 0 0 0},width=0.49\textwidth]{vary_objective.pdf} 
\caption{Cross-entropy loss training and triplet margin loss training are compared for the baseline experiment with DenseNet161. While triplet training does not perform as well as cross-entropy training after five epochs, triplet training likely requires more epochs to finish its optimization. Still, we note that the normalized performance curves are nearly identical.}
\label{fig:vary_triplet_arch}
\end{figure}

\subsection{Varying Classes}
While experimentation on additional overhead image datasets such as SpaceNet and xView would be ideal, fMoW contains such a breadth of imagery that it serves as a strong proxy for any recent multispectral overhead imagery. To account for bias in observing all classes of the dataset simultaneously, we have designed an experiment in which only disjoint subsets of the class space are evaluated.

The results of this experiment are shown in Figure \ref{fig:vary_classes}. As anticipated from Figure \ref{fig:per_class_plots}, partitions of the fMoW subset containing few classes have a more erratic relationship. This result reinforces the point that the classes observed are important to the sensor design problem. We recommend that several different classes are observed in order to make robust sensor design decisions. Figure \ref{fig:vary_classes} indicates that the focal length-rAP relationship becomes stable once around 15 classes are used, for this experiment.

% Plot different num classes: 2, 3, 5, 10, 15, 35
\begin{figure}[!t]
\centering
%\includegraphics[width=\textwidth]{num_class.pdf} 
\includegraphics[trim={0.25cm 0 0 0},width=0.49\textwidth]{num_class.pdf} 
\caption{The fMoW subset is partitioned into 5 sets disjoint by class, containing 2, 3, 5, 10, and 15 of the total classes respectively. The baseline experiment is conducted on each of these subsets and results are compared against the baseline with all 35 classes.}
\label{fig:vary_classes}
\end{figure}

\subsection{Varying Epochs Trained}
Here, we consider how many epochs a model must be trained for in order to converge on a stable focal length-rAP relationship. We consider both training from scratch and fine-tuning of a pre-trained model in Figures \ref{fig:per_epoch_scratch} and \ref{fig:per_epoch_pretrained} respectively. In the from-scratch scenario, we note not only that many more epochs are required to reach a stable relationship, but also that absolute performance is considerably reduced. In the fine-tuning scenario, one training epoch alone may be sufficient to exhibit a stable relationship.

% PLOT per-epoch variation for baseline (scratch)
\begin{figure}[!t]
\centering
\includegraphics[width=0.5\textwidth]{per_epoch_scratch_plots.pdf} 
\caption{The baseline system is trained from scratch, showing changing rAP as a function of focal length for each training epoch. The peak rAP across focal length values for a given epoch is denoted with a triangle. Notice that the peak value shifts towards a smaller focal length as the model is trained for more epochs.}
\label{fig:per_epoch_scratch}
\end{figure}

% PLOT per-epoch variation for baseline (pre-trained)
\begin{figure}[!t]
\centering
\includegraphics[width=0.5\textwidth]{per_epoch_pretrained_plots.pdf} 
\caption{The baseline system is trained from pre-trained weights, showing changing rAP as a function of focal length for each training epoch. The peak rAP across focal length values for a given epoch is denoted with a triangle. Notice that the peak value remains consistent as the network is trained.}
\label{fig:per_epoch_pretrained}
\end{figure}

\subsection{Varying Pre-Processing}
As discussed in Section \ref{subsec:preproc}, we considered two different methods of pre-processing the image data, in order to fit it into the fixed-size format required for the CNN models. We note that most top competitors in the fMoW challenge used a method similar to resizing, but with additional ``context'' pixels around the target bounding box from the original image. While this greatly improves performance for small targets, we wanted to avoid adding additional heuristics to the data preparation process. 

A comparison of performance for the \textit{cropping} and \textit{resizing} methods is shown in Figure \ref{fig:var_proc}. While there is a significant gap in absolute performance, as shown in the plot on the left, the normalized curves on the right are quite similar. This further fortifies our conclusion that there is a self-consistency to visual recognition with CNNs.

% PLOT crop vs. resize
\begin{figure}[!t]
\centering
\includegraphics[trim={0.25cm 0 0 0},width=0.49\textwidth]{crop_resize.pdf} 
\caption{The baseline system is trained with images cropped vs. images resized. Since the images have such a large variation in size, as shown in Figure \ref{fig:fmow_area}, the pre-processing method used for input into the fixed-size network is critical.}
\label{fig:var_proc}
\end{figure}

% PLOT scatter, trend for crop, resize vs. box area

\subsection{Zero-Shot Experiment}
\label{subsec:generalize}
As explained in Section \ref{subsec:experiments}, in this experiment, the fMoW subset is partitioned into two sets disjoint by class, with the training set containing 20 classes, and the validation set containing 15 classes. The model trained on the training set never sees any examples from classes in the validation set. The results are shown in Figures \ref{fig:generalize} and \ref{fig:generalize_triplet} for classifier and triplet training respectively.

This is perhaps the most important experiment conducted in this work, as it shows that the model trained on 20 fMoW classes generalizes to the remaining 15. This suggests that the model could apply to data annotated from any DigitalGlobe sensor, and perhaps from other remote sensors as well. We posit that this fine-tuned model can be used to conduct the sensor parameter space analysis for a wide variety of useful scenarios.

\begin{figure}[!t]
\centering
\includegraphics[trim={0.25cm 0 0 0},width=0.49\textwidth,draft=false]{generalize_classifier.pdf} 
\caption{This plot demonstrates the zero-shot experiment for the baseline case. Four different relationships are shown in this plot: First, the 20-class and 15-class partitions are considered separately. Each was trained and validated on without observing the other. Then, the trained models from the 20-class partition were used to evaluate the 15-class partition- this is the zero-shot case. Notice that these three relationships are very similar and have the same peak for the normalized rAP shown on the right. In addition, performance of the pre-trained ImageNet features on the 15-class set is demonstrated. The result is a relationship clearly unlike the others, indicating that these features may not be salient for this problem.}
\label{fig:generalize}
\end{figure}

% Triplet generlization experiment
\begin{figure}[!t]
\centering
\includegraphics[trim={0.25cm 0 0 0},width=0.49\textwidth,draft=false]{generalize_triplet.pdf} 
\caption{This plot mirrors Figure \ref{fig:generalize} for triplet training instead of classifier training. The normalized relationship is nearly identical, but the zero-shot rAP performance (20 Class Train/15 Class Val) is improved over the classifier case. This is logical, because the features learned with the triplet margin objective are more likely to apply to unseen classes, since the objective doesn't encode for any specific class.}
\label{fig:generalize_triplet}
\end{figure}

\subsection{IQA}
Finally, we compare the performance of the various IQA algorithms for the baseline parameter configuration. The results of this experiment are shown in Figure \ref{fig:iqa}. While both PSNR and BRISQUE have monotonic relationships with respect to increasing focal length, SSIM and NIQE have clear optima in the tested range. Further, the SSIM plot exhibits an optimal Q value, similar to the relationship we saw for CNN performance and NIIRS. 

In Table \ref{tab:optimal_q}, the results from four experimental trials, that varied both focal length and aperture diameter, are summarized to show the Q value for which each metric was optimal. The mean and standard deviation of this Q value are shown, in addition the the minimum and maximum values of the metric measured at this Q value. This result shows that the optimal Q for pre-trained CNN performance is closer to SSIM than to the other metrics. This indicates that out of the four IQA metrics tested, only SSIM may be useful for understanding sensor system performance.

% PLOT IQA
\begin{figure}[!t]
\centering
\includegraphics[width=0.5\textwidth,draft=false]{iqa.pdf} 
\caption{In this plot, we repeat the experiment from Section \ref{subsec:diam} with four different IQA metrics. Six different aperture diameter values are tested, in addition to the varying focal length, and the results are plotted against optical Q. For the full-reference metrics, PSNR increased monotonically, while SSIM has a clear optimal aperture diameter at around Q=0.5. For the no-reference metrics, BRISQUE decreases montonically, while NIQE has an optimal point for each trial, but those optima do not correspond to any specific Q value.}
\label{fig:iqa}
\end{figure}

%\bigskip
\begin{table}
\centering
\captionof{table}{Optimal Q for Different Metrics} \label{tab:optimal_q} 
\begin{tabular*}{0.48\textwidth}{l @{\extracolsep{\fill}} rrrr}
\toprule[1.5pt]
\textbf{Metric} & $\mu(\text{Q})$ & $\sigma(\text{Q})$  & val$_{min}$ & val$_{max}$ \\
\midrule
Scratch rAP* &  0.834 & 0.065 & 0.045 & 0.046 \\
Pre-Trained rAP* &  0.481 & 0.029 & 0.206 & 0.263 \\
GIQE5 NIIRS & 0.840 & 0.010 & 0.892 & 1.477 \\
SSIM & 0.549 & 0.017 & 0.714 & 0.775 \\
\bottomrule
\end{tabular*}
\begin{tablenotes}
\item[*]
This table shows that certain metrics exhibit an optimal value for optical Q when both focal length and aperture diameter were varied.

*Scratch rAP and Pre-Trained rAP refer to the values from Figures \ref{fig:vary_diameter_epoch0} and \ref{fig:vary_diameter_niirs} respectively. 
\end{tablenotes}
\end{table}