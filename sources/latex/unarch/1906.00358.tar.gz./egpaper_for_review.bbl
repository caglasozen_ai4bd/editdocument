\begin{thebibliography}{10}\itemsep=-1pt

\bibitem{ArjovskyCB17}
M.~Arjovsky, S.~Chintala, and L.~Bottou.
\newblock Wasserstein generative adversarial networks.
\newblock In {\em ICML}, 2017.

\bibitem{azadi2018compositional}
S.~Azadi, D.~Pathak, S.~Ebrahimi, and T.~Darrell.
\newblock Compositional {GAN}: Learning conditional image composition.
\newblock {\em arXiv:1807.07560}, 2018.

\bibitem{buda2018systematic}
M.~Buda, A.~Maki, and M.~A. Mazurowski.
\newblock A systematic study of the class imbalance problem in convolutional
  neural networks.
\newblock {\em Neural Networks}, 106:249--259, 2018.

\bibitem{chawla2002smote}
N.~V. Chawla, K.~W. Bowyer, L.~O. Hall, and W.~P. Kegelmeyer.
\newblock {SMOTE}: synthetic minority over-sampling technique.
\newblock {\em JAIR}, 16:321--357, 2002.

\bibitem{mmdetection2018}
K.~Chen, J.~Pang, J.~Wang, Y.~Xiong, X.~Li, S.~Sun, W.~Feng, Z.~Liu, J.~Shi,
  W.~Ouyang, C.~C. Loy, and D.~Lin.
\newblock mmdetection.
\newblock \url{https://github.com/open-mmlab/mmdetection}, 2018.

\bibitem{ChenCDHSSA16}
X.~Chen, Y.~Duan, R.~Houthooft, J.~Schulman, I.~Sutskever, and P.~Abbeel.
\newblock {InfoGAN}: Interpretable representation learning by information
  maximizing generative adversarial nets.
\newblock In {\em NIPS}, 2016.

\bibitem{cheng2018revisiting}
B.~Cheng, Y.~Wei, H.~Shi, S.~Chang, J.~Xiong, and T.~S. Huang.
\newblock Revisiting pre-training: An efficient training method for image
  classification.
\newblock {\em arXiv:1811.09347}, 2018.

\bibitem{cui2019class}
Y.~Cui, M.~Jia, T.-Y. Lin, Y.~Song, and S.~Belongie.
\newblock Class-balanced loss based on effective number of samples.
\newblock {\em arXiv:1901.05555}, 2019.

\bibitem{devries2017dataset}
T.~DeVries and G.~W. Taylor.
\newblock Dataset augmentation in feature space.
\newblock {\em arXiv:1702.05538}, 2017.

\bibitem{devries2017improved}
T.~DeVries and G.~W. Taylor.
\newblock Improved regularization of convolutional neural networks with cutout.
\newblock {\em arXiv:1708.04552}, 2017.

\bibitem{dong2018imbalanced}
Q.~Dong, S.~Gong, and X.~Zhu.
\newblock Imbalanced deep learning by minority class incremental rectification.
\newblock {\em IEEE T-PAMI}, 2018.

\bibitem{dvornik2018modeling}
N.~Dvornik, J.~Mairal, and C.~Schmid.
\newblock Modeling visual context is key to augmenting object detection
  datasets.
\newblock In {\em ECCV}, 2018.

\bibitem{dvornik2018importance}
N.~Dvornik, J.~Mairal, and C.~Schmid.
\newblock On the importance of visual context for data augmentation in scene
  understanding.
\newblock {\em arXiv:1809.02492}, 2018.

\bibitem{dvornik2017blitznet}
N.~Dvornik, K.~Shmelkov, J.~Mairal, and C.~Schmid.
\newblock Blitznet: A real-time deep network for scene understanding.
\newblock In {\em ICCV}, 2017.

\bibitem{dwibedi2017cut}
D.~Dwibedi, I.~Misra, and M.~Hebert.
\newblock Cut, paste and learn: Surprisingly easy synthesis for instance
  detection.
\newblock In {\em ICCV}, 2017.

\bibitem{fu2017dssd}
C.-Y. Fu, W.~Liu, A.~Ranga, A.~Tyagi, and A.~C. Berg.
\newblock {DSSD}: Deconvolutional single shot detector.
\newblock {\em arXiv:1701.06659}, 2017.

\bibitem{georgakis2017synthesizing}
G.~Georgakis, A.~Mousavian, A.~C. Berg, and J.~Kosecka.
\newblock Synthesizing training data for object detection in indoor scenes.
\newblock {\em arXiv:1702.07836}, 2017.

\bibitem{gkioxari2018detecting}
G.~Gkioxari, R.~Girshick, P.~Doll{\'a}r, and K.~He.
\newblock Detecting and recognizing human-object interactions.
\newblock In {\em CVPR}, 2018.

\bibitem{goodfellow2014generative}
I.~Goodfellow, J.~Pouget-Abadie, M.~Mirza, B.~Xu, D.~Warde-Farley, S.~Ozair,
  A.~Courville, and Y.~Bengio.
\newblock Generative adversarial nets.
\newblock In {\em NIPS}, 2014.

\bibitem{gupta2016synthetic}
A.~Gupta, A.~Vedaldi, and A.~Zisserman.
\newblock Synthetic data for text localisation in natural images.
\newblock In {\em CVPR}, 2016.

\bibitem{he2008learning}
H.~He and E.~A. Garcia.
\newblock Learning from imbalanced data.
\newblock {\em IEEE T-K\&DE}, (9):1263--1284, 2008.

\bibitem{he2017mask}
K.~He, G.~Gkioxari, P.~Doll{\'a}r, and R.~Girshick.
\newblock Mask {R-CNN}.
\newblock In {\em ICCV}, 2017.

\bibitem{HeGDG18}
K.~He, G.~Gkioxari, P.~Doll{\'{a}}r, and R.~B. Girshick.
\newblock Mask {R-CNN}.
\newblock {\em IEEE T-PAMI}, 2018.

\bibitem{he2016deep}
K.~He, X.~Zhang, S.~Ren, and J.~Sun.
\newblock Deep residual learning for image recognition.
\newblock In {\em CVPR}, 2016.

\bibitem{huang2016learning}
C.~Huang, Y.~Li, C.~Change~Loy, and X.~Tang.
\newblock Learning deep representation for imbalanced classification.
\newblock In {\em CVPR}, 2016.

\bibitem{huang2017densely}
G.~Huang, Z.~Liu, L.~Van Der~Maaten, and K.~Q. Weinberger.
\newblock Densely connected convolutional networks.
\newblock In {\em CVPR}, 2017.

\bibitem{inoue2018data}
H.~Inoue.
\newblock Data augmentation by pairing samples for images classification.
\newblock {\em arXiv:1801.02929}, 2018.

\bibitem{khan2018cost}
S.~H. Khan, M.~Hayat, M.~Bennamoun, F.~A. Sohel, and R.~Togneri.
\newblock Cost-sensitive learning of deep feature representations from
  imbalanced data.
\newblock {\em IEEE T-NNLS}, 29(8), 2018.

\bibitem{kumra2017robotic}
S.~Kumra and C.~Kanan.
\newblock Robotic grasp detection using deep convolutional neural networks.
\newblock In {\em IROS}, 2017.

\bibitem{lalonde2007using}
J.-F. Lalonde and A.~A. Efros.
\newblock Using color compatibility for assessing image realism.
\newblock In {\em ICCV}, 2007.

\bibitem{lee2018context}
D.~Lee, S.~Liu, J.~Gu, M.-Y. Liu, M.-H. Yang, and J.~Kautz.
\newblock Context-aware synthesis and placement of object instances.
\newblock In {\em NeurIPS}, 2018.

\bibitem{lin2017feature}
T.-Y. Lin, P.~Doll{\'a}r, R.~B. Girshick, K.~He, B.~Hariharan, and S.~J.
  Belongie.
\newblock Feature pyramid networks for object detection.
\newblock In {\em CVPR}, 2017.

\bibitem{lin2017focal}
T.-Y. Lin, P.~Goyal, R.~Girshick, K.~He, and P.~Doll{\'a}r.
\newblock Focal loss for dense object detection.
\newblock In {\em ICCV}, 2017.

\bibitem{lin2014microsoft}
T.-Y. Lin, M.~Maire, S.~Belongie, J.~Hays, P.~Perona, D.~Ramanan,
  P.~Doll{\'a}r, and C.~L. Zitnick.
\newblock {Microsoft COCO}: Common objects in context.
\newblock In {\em ECCV}, 2014.

\bibitem{liu2016ssd}
W.~Liu, D.~Anguelov, D.~Erhan, C.~Szegedy, S.~Reed, C.-Y. Fu, and A.~C. Berg.
\newblock {SSD}: Single shot multibox detector.
\newblock In {\em ECCV}, 2016.

\bibitem{ranjan2019hyperface}
R.~Ranjan, V.~M. Patel, and R.~Chellappa.
\newblock Hyperface: A deep multi-task learning framework for face detection,
  landmark localization, pose estimation, and gender recognition.
\newblock {\em IEEE T-PAMI}, 41(1), 2019.

\bibitem{remez2018learning}
T.~Remez, J.~Huang, and M.~Brown.
\newblock Learning to segment via cut-and-paste.
\newblock In {\em ECCV}, 2018.

\bibitem{ren2015faster}
S.~Ren, K.~He, R.~Girshick, and J.~Sun.
\newblock Faster {R-CNN}: Towards real-time object detection with region
  proposal networks.
\newblock In {\em NIPS}, 2015.

\bibitem{sarafianos2018deep}
N.~Sarafianos, X.~Xu, and I.~A. Kakadiaris.
\newblock Deep imbalanced attribute classification using visual attention
  aggregation.
\newblock In {\em ECCV}, 2018.

\bibitem{ShrivastavaPTSW17}
A.~Shrivastava, T.~Pfister, O.~Tuzel, J.~Susskind, W.~Wang, and R.~Webb.
\newblock Learning from simulated and unsupervised images through adversarial
  training.
\newblock In {\em CVPR}, 2017.

\bibitem{simonyan2014very}
K.~Simonyan and A.~Zisserman.
\newblock Very deep convolutional networks for large-scale image recognition.
\newblock {\em arXiv:1409.1556}, 2014.

\bibitem{singh2018sniper}
B.~Singh, M.~Najibi, and L.~S. Davis.
\newblock {SNIPER}: Efficient multi-scale training.
\newblock In {\em NeurIPS}, 2018.

\bibitem{szegedy2017inception}
C.~Szegedy, S.~Ioffe, V.~Vanhoucke, and A.~A. Alemi.
\newblock Inception-v4, inception-resnet and the impact of residual connections
  on learning.
\newblock In {\em AAAI}, 2017.

\bibitem{takahashi2018data}
R.~Takahashi, T.~Matsubara, and K.~Uehara.
\newblock Data augmentation using random image cropping and patching for deep
  cnns.
\newblock {\em arXiv:1811.09030}, 2018.

\bibitem{OordKK16}
A.~van~den Oord, N.~Kalchbrenner, and K.~Kavukcuoglu.
\newblock Pixel recurrent neural networks.
\newblock In {\em ICML}, 2016.

\bibitem{0004SG17}
X.~Wang, A.~Shrivastava, and A.~Gupta.
\newblock {A-Fast-RCNN}: Hard positive generation via adversary for object
  detection.
\newblock In {\em CVPR}, 2017.

\bibitem{wang2019dynamic}
Y.~Wang, W.~Gan, W.~Wu, and J.~Yan.
\newblock Dynamic curriculum learning for imbalanced data classification.
\newblock {\em arXiv:1901.06783}, 2019.

\bibitem{zhang2017mixup}
H.~Zhang, M.~Cisse, Y.~N. Dauphin, and D.~Lopez-Paz.
\newblock mixup: Beyond empirical risk minimization.
\newblock {\em ICLR}, 2017.

\bibitem{zhong2017random}
Z.~Zhong, L.~Zheng, G.~Kang, S.~Li, and Y.~Yang.
\newblock Random erasing data augmentation.
\newblock {\em arXiv:1708.04896}, 2017.

\bibitem{zhu2015learning}
J.-Y. Zhu, P.~Krahenbuhl, E.~Shechtman, and A.~A. Efros.
\newblock Learning a discriminative model for the perception of realism in
  composite images.
\newblock In {\em ICCV}, 2015.

\end{thebibliography}
