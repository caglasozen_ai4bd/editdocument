\begin{thebibliography}{10}\itemsep=-1pt

\bibitem{abadi2016tensorflow}
M.~Abadi, P.~Barham, J.~Chen, Z.~Chen, A.~Davis, J.~Dean, M.~Devin,
  S.~Ghemawat, G.~Irving, M.~Isard, et~al.
\newblock Tensorflow: A system for large-scale machine learning.

\bibitem{10.1371/journal.pcbi.1005743}
E.~Akbas and M.~P. Eckstein.
\newblock Object detection through search with a foveated visual system.
\newblock {\em PLOS Computational Biology}, 13(10):1--28, 10 2017.

\bibitem{arevalo2016representation}
J.~Arevalo, F.~A. Gonz{\'a}lez, R.~Ramos-Poll{\'a}n, J.~L. Oliveira, and
  M.~A.~G. Lopez.
\newblock Representation learning for mammography mass lesion classification
  with convolutional neural networks.
\newblock {\em Computer methods and programs in biomedicine}, 127:248--257,
  2016.

\bibitem{branson2014ignorant}
S.~Branson, G.~Van~Horn, C.~Wah, P.~Perona, and S.~Belongie.
\newblock The ignorant led by the blind: A hybrid human--machine vision system
  for fine-grained categorization.
\newblock {\em International Journal of Computer Vision}, 108(1-2):3--29, 2014.

\bibitem{canny1987computational}
J.~Canny.
\newblock A computational approach to edge detection.
\newblock In {\em Readings in Computer Vision}, pages 184--203. Elsevier, 1987.

\bibitem{chattopadhyay2017evaluating}
P.~Chattopadhyay, D.~Yadav, V.~Prabhu, A.~Chandrasekaran, A.~Das, S.~Lee,
  D.~Batra, and D.~Parikh.
\newblock Evaluating visual conversational agents via cooperative human-ai
  games.
\newblock {\em arXiv preprint arXiv:1708.05122}, 2017.

\bibitem{cheung2016emergence}
B.~Cheung, E.~Weiss, and B.~Olshausen.
\newblock Emergence of foveal image sampling from learning to attend in visual
  scenes.
\newblock {\em arXiv preprint arXiv:1611.09430}, 2016.

\bibitem{dai2016r}
J.~Dai, Y.~Li, K.~He, and J.~Sun.
\newblock R-fcn: Object detection via region-based fully convolutional
  networks.
\newblock In {\em Advances in neural information processing systems}, pages
  379--387, 2016.

\bibitem{dalal2005histograms}
N.~Dalal and B.~Triggs.
\newblock Histograms of oriented gradients for human detection.
\newblock In {\em Computer Vision and Pattern Recognition, 2005. CVPR 2005.
  IEEE Computer Society Conference on}, volume~1, pages 886--893. IEEE, 2005.

\bibitem{das2017human}
A.~Das, H.~Agrawal, L.~Zitnick, D.~Parikh, and D.~Batra.
\newblock Human attention in visual question answering: Do humans and deep
  networks look at the same regions?
\newblock {\em Computer Vision and Image Understanding}, 163:90--100, 2017.

\bibitem{deza2016can}
A.~Deza and M.~P. Eckstein.
\newblock Can peripheral representations improve clutter metrics on complex
  scenes?
\newblock In {\em Neural Information Processing Systems}, 2016.

\bibitem{deza2017attention}
A.~Deza, J.~R. Peters, G.~S. Taylor, A.~Surana, and M.~P. Eckstein.
\newblock Attention allocation aid for visual search.
\newblock {\em arXiv preprint arXiv:1701.03968}, 2017.

\bibitem{eckstein2011visual}
M.~P. Eckstein.
\newblock Visual search: A retrospective.
\newblock {\em Journal of vision}, 11(5):14--14, 2011.

\bibitem{eckstein2017humans}
M.~P. Eckstein, K.~Koehler, L.~E. Welbourne, and E.~Akbas.
\newblock Humans, but not deep neural networks, often miss giant targets in
  scenes.
\newblock {\em Current Biology}, 27(18):2827--2832, 2017.

\bibitem{eggert2017improving}
C.~Eggert, D.~Zecha, S.~Brehm, and R.~Lienhart.
\newblock Improving small object proposals for company logo detection.
\newblock In {\em Proceedings of the 2017 ACM on International Conference on
  Multimedia Retrieval}, pages 167--174. ACM, 2017.

\bibitem{elsayed2018adversarial}
G.~F. Elsayed, S.~Shankar, B.~Cheung, N.~Papernot, A.~Kurakin, I.~Goodfellow,
  and J.~Sohl-Dickstein.
\newblock Adversarial examples that fool both human and computer vision.
\newblock {\em arXiv preprint arXiv:1802.08195}, 2018.

\bibitem{esteva2017dermatologist}
A.~Esteva, B.~Kuprel, R.~A. Novoa, J.~Ko, S.~M. Swetter, H.~M. Blau, and
  S.~Thrun.
\newblock Dermatologist-level classification of skin cancer with deep neural
  networks.
\newblock {\em Nature}, 542(7639):115, 2017.

\bibitem{felzenszwalb2010object}
P.~F. Felzenszwalb, R.~B. Girshick, D.~McAllester, and D.~Ramanan.
\newblock Object detection with discriminatively trained part-based models.
\newblock {\em IEEE transactions on pattern analysis and machine intelligence},
  32(9):1627--1645, 2010.

\bibitem{finlayson2018adversarial}
S.~G. Finlayson, I.~S. Kohane, and A.~L. Beam.
\newblock Adversarial attacks against medical deep learning systems.
\newblock {\em arXiv preprint arXiv:1804.05296}, 2018.

\bibitem{geirhos2017comparing}
R.~Geirhos, D.~H. Janssen, H.~H. Sch{\"u}tt, J.~Rauber, M.~Bethge, and F.~A.
  Wichmann.
\newblock Comparing deep neural networks against humans: object recognition
  when the signal gets weaker.
\newblock {\em arXiv preprint arXiv:1706.06969}, 2017.

\bibitem{goodfellow2014explaining}
I.~J. Goodfellow, J.~Shlens, and C.~Szegedy.
\newblock Explaining and harnessing adversarial examples.
\newblock {\em arXiv preprint arXiv:1412.6572}, 2014.

\bibitem{green1988signal}
J.~GREEN Dand~SWETS.
\newblock Signal detection theory and psychophysics, 1988.

\bibitem{hariharan2017object}
B.~Hariharan, P.~Arbelaez, R.~Girshick, and J.~Malik.
\newblock Object instance segmentation and fine-grained localization using
  hypercolumns.
\newblock {\em IEEE transactions on pattern analysis and machine intelligence},
  39(4):627--639, 2017.

\bibitem{huang2017speed}
J.~Huang, V.~Rathod, C.~Sun, M.~Zhu, A.~Korattikara, A.~Fathi, I.~Fischer,
  Z.~Wojna, Y.~Song, S.~Guadarrama, et~al.
\newblock Speed/accuracy trade-offs for modern convolutional object detectors.

\bibitem{JohnsCVPR2015}
E.~Johns, O.~Mac~Aodha, and G.~J. Brostow.
\newblock {Becoming the Expert - Interactive Multi-Class Machine Teaching}.
\newblock In {\em CVPR}, 2015.

\bibitem{kneusel2017improving}
R.~T. Kneusel and M.~C. Mozer.
\newblock Improving human-machine cooperative visual search with soft
  highlighting.
\newblock {\em ACM Transactions on Applied Perception (TAP)}, 15(1):3, 2017.

\bibitem{kooi2017large}
T.~Kooi, G.~Litjens, B.~van Ginneken, A.~Gubern-M{\'e}rida, C.~I. S{\'a}nchez,
  R.~Mann, A.~den Heeten, and N.~Karssemeijer.
\newblock Large scale deep learning for computer aided detection of
  mammographic lesions.
\newblock {\em Medical image analysis}, 35:303--312, 2017.

\bibitem{krupinski2010current}
E.~A. Krupinski.
\newblock Current perspectives in medical image perception.
\newblock {\em Attention, Perception, \& Psychophysics}, 72(5):1205--1217,
  2010.

\bibitem{litjens2017survey}
G.~Litjens, T.~Kooi, B.~E. Bejnordi, A.~A.~A. Setio, F.~Ciompi, M.~Ghafoorian,
  J.~A. van~der Laak, B.~van Ginneken, and C.~I. S{\'a}nchez.
\newblock A survey on deep learning in medical image analysis.
\newblock {\em Medical image analysis}, 42:60--88, 2017.

\bibitem{liu2016ssd}
W.~Liu, D.~Anguelov, D.~Erhan, C.~Szegedy, S.~Reed, C.-Y. Fu, and A.~C. Berg.
\newblock Ssd: Single shot multibox detector.
\newblock In {\em European conference on computer vision}, pages 21--37.
  Springer, 2016.

\bibitem{malik2016three}
J.~Malik, P.~Arbel{\'a}ez, J.~Carreira, K.~Fragkiadaki, R.~Girshick,
  G.~Gkioxari, S.~Gupta, B.~Hariharan, A.~Kar, and S.~Tulsiani.
\newblock The three r’s of computer vision: Recognition, reconstruction and
  reorganization.
\newblock {\em Pattern Recognition Letters}, 72:4--14, 2016.

\bibitem{perez2003poisson}
P.~P{\'e}rez, M.~Gangnet, and A.~Blake.
\newblock Poisson image editing.
\newblock {\em ACM Transactions on graphics (TOG)}, 22(3):313--318, 2003.

\bibitem{peters2015human}
J.~R. Peters, V.~Srivastava, G.~S. Taylor, A.~Surana, M.~P. Eckstein, and
  F.~Bullo.
\newblock Human supervisory control of robotic teams: integrating cognitive
  modeling with engineering design.
\newblock {\em IEEE Control Systems}, 35(6):57--80, 2015.

\bibitem{pramod2016computational}
R.~Pramod and S.~Arun.
\newblock Do computational models differ systematically from human object
  perception?
\newblock In {\em Proceedings of the IEEE Conference on Computer Vision and
  Pattern Recognition}, pages 1601--1609, 2016.

\bibitem{rajpurkar2017chexnet}
P.~Rajpurkar, J.~Irvin, K.~Zhu, B.~Yang, H.~Mehta, T.~Duan, D.~Ding, A.~Bagul,
  C.~Langlotz, K.~Shpanskaya, et~al.
\newblock Chexnet: Radiologist-level pneumonia detection on chest x-rays with
  deep learning.
\newblock {\em arXiv preprint arXiv:1711.05225}, 2017.

\bibitem{redmon2015you}
J.~Redmon, S.~Divvala, R.~Girshick, and A.~Farhadi.
\newblock You only look once: Unified, real-time object detection.
\newblock {\em arXiv preprint arXiv:1506.02640}, 2015.

\bibitem{redmon2017yolo9000}
J.~Redmon and A.~Farhadi.
\newblock Yolo9000: better, faster, stronger.

\bibitem{ren2015faster}
S.~Ren, K.~He, R.~Girshick, and J.~Sun.
\newblock Faster r-cnn: Towards real-time object detection with region proposal
  networks.
\newblock In {\em Advances in neural information processing systems}, pages
  91--99, 2015.

\bibitem{russakovsky2015best}
O.~Russakovsky, L.-J. Li, and L.~Fei-Fei.
\newblock Best of both worlds: human-machine collaboration for object
  annotation.
\newblock In {\em Proceedings of the IEEE Conference on Computer Vision and
  Pattern Recognition}, pages 2121--2131, 2015.

\bibitem{simard2017machine}
P.~Y. Simard, S.~Amershi, D.~M. Chickering, A.~E. Pelton, S.~Ghorashi, C.~Meek,
  G.~Ramos, J.~Suh, J.~Verwey, M.~Wang, et~al.
\newblock Machine teaching: A new paradigm for building machine learning
  systems.
\newblock {\em arXiv preprint arXiv:1707.06742}, 2017.

\bibitem{simonyan2014very}
K.~Simonyan and A.~Zisserman.
\newblock Very deep convolutional networks for large-scale image recognition.
\newblock {\em arXiv preprint arXiv:1409.1556}, 2014.

\bibitem{wolfe2017more}
B.~Wolfe, J.~Dobres, R.~Rosenholtz, and B.~Reimer.
\newblock More than the useful field: considering peripheral vision in driving.
\newblock {\em Applied ergonomics}, 65:316--325, 2017.

\end{thebibliography}
