\begin{thebibliography}{10}

\bibitem{Partalasa}
Partalas, I., Kosmopoulos, A., Baskiotis, N., Artieres, T., Paliouras, G.,
  Gaussier, E., Androutsopoulos, I., Amini, M.r., Galinari, P.:
\newblock {Large Scale Hierarchical Text Classification Challenge : A Benchmark
  for Large-Scale Text Classification}.
\newblock arXiv:1503.08581v1 (2015)

\bibitem{ILSVRC15}
Russakovsky, O., Deng, J., Su, H., Krause, J., Satheesh, S., Ma, S., Huang, Z.,
  Karpathy, A., Khosla, A., Bernstein, M., Berg, A.C., Fei-Fei, L.:
\newblock {ImageNet Large Scale Visual Recognition Challenge}.
\newblock International Journal of Computer Vision \textbf{115}(3) (2015)
  211--252

\bibitem{Perronnin2012b}
Akata, Z., Perronnin, F., Harchaoui, Z., Schmid, C.:
\newblock {Good Practice in Large-Scale Learning for Image Classification}.
\newblock {IEEE Transactions on Pattern Analysis and Machine Intelligence}
  \textbf{36}(3) (2014)  507--520

\bibitem{BengioLC13}
Bengio, Y., L{\'{e}}onard, N., Courville, A.C.:
\newblock Estimating or propagating gradients through stochastic neurons for
  conditional computation.
\newblock arXiv:1308.3432 (2013)

\bibitem{bengio2010}
Bengio, S., Weston, J., Grangier, D.:
\newblock Label embedding trees for large multi-class tasks.
\newblock In: Advances in Neural Information Processing Systems 23.
\newblock (2010)  163--171

\bibitem{Weston2013}
Weston, J., Makadia, A., Yee, H.:
\newblock Label partitioning for sublinear ranking.
\newblock In: Proc. of the 30th International Conference on Machine Learning
  (ICML-13). Volume~28. (2013)  181--189

\bibitem{puget2015hierarchical}
Puget, R., Baskiotis, N.:
\newblock Hierarchical label partitioning for large scale classification.
\newblock In: {IEEE} International Conference on Data Science and Advanced
  Analytics, {DSAA}. (2015)  1--10

\bibitem{Dietterich1995}
Dietterich, T.G., Bakiri, G.:
\newblock Solving multiclass learning problems via error-correcting output
  codes.
\newblock J. of Artificial Intelligence Research \textbf{2} (1995)  263--286

\bibitem{Zhong13}
Zhong, G., Cheriet, M.:
\newblock Adaptive error-correcting output codes.
\newblock In: Proceedings of the Twenty-Third International Joint Conference on
  Artificial Intelligence. IJCAI '13, AAAI Press (2013)  1932--1938

\bibitem{cisse2012}
Ciss{\'e}, M., Arti{\`e}res, T., Gallinari, P.:
\newblock Learning compact class codes for fast inference in large multi class
  classification.
\newblock In: Machine Learning and Knowledge Discovery in Databases: ECML PKDD.
\newblock Springer Berlin Heidelberg (2012)  506--520

\bibitem{Norouzi14}
Norouzi, M., Punjani, A., Fleet, D.J.:
\newblock Fast exact search in hamming space with multi-index hashing.
\newblock IEEE Transactions on Pattern Analysis and Machine Intelligence
  \textbf{36}(6) (2014)  1107--1119

\bibitem{Gionis99}
Gionis, A., Indyk, P., Motwani, R.:
\newblock Similarity search in high dimensions via hashing.
\newblock In: Proceedings of the 25th International Conference on Very Large
  Data Bases. VLDB '99, Morgan Kaufmann Publishers Inc. (1999)  518--529

\bibitem{Weiss09}
Weiss, Y., Torralba, A., Fergus, R.:
\newblock Spectral hashing.
\newblock In: Advances in Neural Information Processing Systems 21.
\newblock (2009)  1753--1760

\bibitem{Salakhutdinov09}
Salakhutdinov, R., Hinton, G.:
\newblock Semantic hashing.
\newblock International Journal of Approximate Reasoning \textbf{50}(7) (July
  2009)  969--978

\bibitem{Lai15}
Lai, H., Pan, Y., Liu, Y., Yan, S.:
\newblock Simultaneous feature learning and hash coding with deep neural
  networks.
\newblock arXiv:1504.03410 (2015)

\bibitem{Do2016}
Do, T.T., Doan, A.D., Cheung, N.M.:
\newblock Learning to hash with binary deep neural network.
\newblock In: Computer Vision -- ECCV 2016: 14th European Conference,
  Proceedings, Part V.
\newblock Springer International Publishing, Cham (2016)  219--234

\bibitem{Wang17}
Wang, J., Zhang, T., Sebe, N., Shen, H.T.,  et~al.:
\newblock A survey on learning to hash.
\newblock IEEE Trans. on Pattern Analysis and Machine Intelligence \textbf{- to
  appear} (2017)

\bibitem{Liong15}
Erin~Liong, V., Lu, J., Wang, G., Moulin, P., Zhou, J.:
\newblock Deep hashing for compact binary codes learning.
\newblock In: The IEEE Conference on Computer Vision and Pattern Recognition
  (CVPR). (2015)

\bibitem{Williams1992}
Williams, R.J.:
\newblock Simple statistical gradient-following algorithms for connectionist
  reinforcement learning.
\newblock Machine Learning \textbf{8}(3) (1992)  229--256

\bibitem{Galar:2013:DCS:2514172.2514280}
Galar, M., Fern{\'a}ndez, A., Barrenechea, E., Bustince, H., Herrera, F.:
\newblock Dynamic classifier selection for one-vs-one strategy: avoiding
  non-competent classifiers.
\newblock Pattern Recognition \textbf{46}(12) (2013)  3412--3424

\bibitem{He2015}
He, K., Zhang, X., Ren, S., Sun, J.:
\newblock Deep residual learning for image recognition.
\newblock arXiv:1512.03385 (2015)

\bibitem{DBLP:journals/corr/KingmaB14}
Kingma, D.P., Ba, J.:
\newblock Adam: {A} method for stochastic optimization.
\newblock arXiv:1412.6980 (2014)

\end{thebibliography}
