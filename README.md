#README

##Current State

You can execute the program when you run the main.py function or the testMain.py function. It takes time for the program to execute, so first of all you'll have to make sure that you have waited enough time. However the code might get stuck somewhere if it is buggy for the given document. Apart from the input specific bugs, after the execution of the program for several documents, the OS might generate an interrupt (SIGKILL) and stop the execution. But this is not like an exception that can be handled. So, I could neither find a solution for this case, nor found the reason why this occurs. For succesful executions, an out folder is created in the outermost directory of the project and the folder containing the latex file is moved to that folder.

Currently, the program is buggy although I have solved numerous bugs. But almost all of these bugs are not a result of the code itself but rather a result of the latex source codes themselves. These problems are written in a documenht called "~Problems Cagla faced during her tasks~" in the same folder as this document.



##Dependencies

For being able to run the program on any computer, there are some libraries and latex packages that are needed to be installed on the machine. The following commands can be copied and pasted to the command line of a machine (Ubuntu 19.04 was my OS).

- Installing Python and texlive

>sudo apt-get update
>
>sudo apt-get install texlive-full
>
>sudo apt-get install python3.6

- Installing Anaconda
	- Retrieve the latest version of Anaconda from
> https://www.anaconda.com/distribution/
	-Then execute the following commands
> cd /tmp
>
> curl -O https://repo.anaconda.com/archive/Anaconda3-2019.03-Linux-x86_64.sh
>
> bash Anaconda3-2019.03-Linux-x86_64.sh
>
> source ~/.bashrc
>
> conda list
>
> conda create --name my_env python=3
>
> conda activate my_env

- Installing OpenCV

>cd ~/<my_working_directory>
>
>git clone https://github.com/opencv/opencv.git
>
>git clone https://github.com/opencv/opencv_contrib.git
>
>cd ~/opencv
>
>mkdir build
>
>cd build
>
>cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=/usr/local ..
>
>make -j7 # runs 7 jobs in parallel
>
>cd ~/opencv/build/doc/
>
>make -j7 doxygen
>
>sudo make install

- Install opencv-python

>pip install opencv-python
>
>pip install opencv-python-headless


You will need to install the following packages, from here on

![Required packages](assets/packages.png "PackageImage")

##Changes required in the code

###main.py
ARXIV_UNARCH_PATH:  Output folder for the unarchived files.
OUT_PATH: Output folder for the succesfully edited files.
scp_path: Path to the sourcecode of the unarchival cmd script

###unarchScript

Change all paths according to your computer. I accidentally affected a setting regarding relative paths and couldn't use relative paths from that point on. Sorry :(

##Program Flow


![Program Flow](assets/flow.png "ProgramFlow")

- Code flow consists of 7 classes and a cmd script.
- Main function utilizes the other classes.
- For a succesful execution, this function moves the succesfully processed document's folder to an outer folder called out.
- Also, this function calculates the average time spent for processing of the documents.

- **unarchUtil** class is called, inside this class unarchScript is called. As a result of this execution, the given arxiv file is unarchived into folders for each Latex document.

    *For each of the paths outputted by the unarchUtil execution*

        - **modifyTableText** is called to edit the source code of the Latex file for coloring the tables inside the table. Also, this class compiles the latex file and creates PNG's. As a result of this execution, we obtain two PNG's for each page in the latex document, one black and white and one colored.

        *If the compilation was succesful*
    
            - **extractTableInfo** is called to extract information about the tables beforehand from the source code. As a result of this execution, we obtain a JSON file with table information in it.
    
            - **detectTable** is called to extract location information of the tables' according to the positioning in the pages. It utilizes the JSON file outputted by the execution of extractTableInfo and the PNG files outputted by modifyTableText. Outputs a JSON file containing the location information of the cells and tables.
    
            - **toCOCO** is called to create a COCO formatted file using the JSON file outputted by the execution of detectTable. Outputs a JSON file formatted in MS-COCO format.
    
            - **extractRelation** is called to create two JSON files relations.json and objects.json for denoting the relations between table elements (objects). Utilized the JSON file outputted by toCOCO and the JSON file outputted by detectTable. Outputs two JSON files relations.json and objects.json.    

##Warnings

- Execution takes time, please make sure you've waited enough time. 
- This program edits the latex source codes so it can not be called over the same file over and over again. Make sure you use the untagged, original file when you rerun the program. 
- There are several tar warnings in the beginning of the program, I couldn't avoid them but they do not harm the execution. 

##Notes

testMain.py can be used for testing the execution of the program for a single file. You can change the path in the code to the path of the tex file to be compiled within the code. 


