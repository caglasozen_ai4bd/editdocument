#!/usr/bin/env conda

# A class to color the table text in a Latex document in a different RGB value for each color. This process also removes the comments in a document. Then recompiles latex, obtaines the PDF and exports to JPEG and PNG formats.
# @author: Cagla Sozen (Intern)
# @version: 1.1 (After Code Review)
# @date: 28.08.2019


import re

import os

from pdf2image import convert_from_path


class ModifyTextUtil:

    OFFSET = 15  #Offset for skipping the enddocument tag
    red_perc = 255
    green_perc = 255
    blue_perc = 255
    has_table = False #Flag to check whether the document contains a table or not.

    def __init__(self, path_e, path_c):
        self.path_e = path_e
        self.path_c = path_c

    # Method to get the content of a document using the file pointer.
    # @param: File pointer to the regarding file.
    # @return: String contents of the document.

    def get_contents(self, f):

        contents = ""

        if f.mode == 'r':
            contents = f.read()
            f.close()

        return contents

    # Method to add the necessary packages to the main latex source code.
    # @param: String contents of the document
    # @return: String the edited form of contents
    def add_package(self, contents):

        if contents.find("\\usepackage{xcolor}") == -1:

            lib_tag = "\\usepackage{xcolor} \n "

            begin = contents.find("\\begin{document}")

            if begin != -1:
                contents = contents[:begin] + lib_tag + contents[begin:]  ##here i add the tag to be able to use the library

        return contents

    # Method to remove comments from a given content.
    # @param: String contents of the document.
    # @return: String the edited form of contents
    def rem_comment(self, lines):

        try:
            comm_st = lines.find("%")

            while comm_st != -1:
                comm_end = lines.find("\n", comm_st)

                if lines[comm_st-1] == "\\":
                    comm_st = lines.find("%", comm_st+1)
                    continue

                lines = lines[:comm_st] + "\n" + lines[comm_end+1:]
                comm_st = lines.find("%")
        except MemoryError:
            print("MemoryError")

        return lines

    # Method to compile the latex file in a given path.
    # @param: String path of the file to be compiled.
    # @return: String tuple, the first is the path to the compiled tex file and the second is the path to the directory holding the main tex file.
    def compile_latex(self, path):

        ind = path.rfind("/")
        go_path = path[:ind]

        path = path[ind + 1:]

        for i in range(2):
            os.chdir(go_path)
            ex = os.system("pdflatex -halt-on-error " + path + " >/dev/null ")
            if ex != 0:
                print("LATEX ERROR")
                return None
        return path, go_path

    # Method to convert all pages in a PDF document into PNG.
    # @param: String path to the output directory
    # @param: String path to the hosting directory
    # @param: String name of the output files.
    def to_png(self, out_path, go_path, pdf_path, name):
        try:
            os.mkdir(out_path)
        except OSError:
            print("Unable to create directory %s" % out_path)
        else:
            print("Created directory %s " % out_path)

        out_path = go_path + "/png"

        pages = convert_from_path(pdf_path, 500)
        p_no = 0
        for page in pages:
            print("Writing...")
            f_name = out_path + "/" +  name + "-" + str(p_no) + ".png"
            page.save(f_name, 'PNG')
            p_no = p_no + 1

    # Method to extract the tables in a latex file and edit their source code with textcolor content to color each cell content with a different rgb value.
    # @param: String contents to be edited.
    # @param: int index to start from to locate the table.
    # @param: boolean a flag to indicate the color values to use. Whether it will be all black or varying colors.
    # @return: (String,int) tuple, the first is the last index of the table found, the second is the edited contents.
    def split_tabular(self, contents, ind, flag):

        beg_ind = contents.find("\\begin{tabular}", ind)

        end_ind = contents.find("\\end{tabular}", ind) + self.OFFSET

        size = len(contents[beg_ind:end_ind])

        if (beg_ind != -1) and (end_ind != -1):

            self.has_table = True

            mod_line = self.modify_cells(contents[beg_ind:end_ind],flag)

            beg_cont = contents[:beg_ind]
            end_cont = contents[end_ind:]

            size = len(mod_line) - size

            contents = beg_cont + mod_line + end_cont

        return end_ind+size, contents

    # A helper method to edit the contents of a line extracted from a table by the split_tabular() method. Adds the textcolor tags to each line.
    # @param: String line to be edited.
    # @param: boolean flag to indicate whether the text will be colored in black or varying colors.
    # @return: String edited line
    def modify_cells(self, line, flag):
        i = 0
        mod_line = ""
        add = 1

        while re.search("(.+?&)|((.+\\\\))\s", line) is not None:

            mid_tag_str = line.find("\\midrule", 9)
            hline_tag_str = line.find("\\hline", 7)

            mid_tag_end = line.find("\\midrule", mid_tag_str + 1)
            hline_tag_end = line.find("\\hline", hline_tag_str + 1)

            if(mid_tag_str == -1):
                mid_tag_end = line.find("\\midrule",mid_tag_str+10)
            elif( hline_tag_str == -1 ):
                hline_tag_end = line.find("\\hline", hline_tag_str + 8)

            tag_no = 1

            if (mid_tag_end != -1 and mid_tag_str != -1) or (hline_tag_end != -1 and hline_tag_str != -1):
                tag_no = 2
            elif mid_tag_end == -1 and mid_tag_str == -1:
                tag_no = 0
            else:
                tag_no = 1

            real_content = re.search("(.+?&)|((.+\\\\))\s", line)
            if (real_content is None):
                continue
            indeces = real_content.span()
            cont_string = real_content.group(0)

            end  = indeces[1]
            begin = indeces[0]

            if re.search("&",cont_string) is not None:
                add = 1
            else:
                add = 3

            ex_bra_left = cont_string.find("{")
            ex_bra_right = cont_string.find("}")

            norm = line.find(cont_string)

            while ex_bra_left != -1 and ex_bra_right != -1:
                begin = norm + ex_bra_left +1
                end =  norm + ex_bra_right + 1

                ex_bra_left = cont_string.find("{",ex_bra_left+1)
                ex_bra_right = cont_string.rfind("}",ex_bra_right-1)

            if flag is True:
                tag_perc = str(0) + "," + str(0) + "," + str(0)
            else:
                if tag_no is 2:
                    self.red_perc = (self.red_perc - 7) % 255
                    tag_perc = str(self.red_perc) + "," + str(255) + "," + str(255)
                elif tag_no is 0:
                    self.green_perc = (self.green_perc - 7) % 255
                    tag_perc = str(255) + "," + str(self.green_perc)  + "," + str(255)
                else:
                    self.blue_perc = (self.blue_perc - 7) % 255
                    tag_perc = str(255) + "," + str(255)  + "," + str(self.blue_perc)


            colortag_beg = "\\textcolor[RGB]{" + tag_perc + "}{"
            colortag_end = "} "

            mod_line = mod_line + line[:begin] + colortag_beg + line[begin:end - add] + colortag_end + line[end-add:end + 1]

            line = line[(end + 1):]

        return mod_line + line

    # Method to split parts of a given path.
    # @param: String path to be splitted.
    # @return: String list, the first is path to the png output path, second is the path to the hosting directory, last one is the path to the pdf created from latex compilation.
    def split_paths(self, paths):
        path = paths[0]
        go_path = paths[1]

        new_ind = path.rfind(".")

        pdf_path = path[:new_ind] + ".pdf"

        out_path = go_path + "/png"

        return out_path, go_path, pdf_path

    # Method to write the given contents to a document in the given path.
    # @param: String path to the document to be written on.
    # @param: String contents to be edited.
    def write_doc(self, path,contents):
        file_wrt = open(path, "w")

        file_wrt.write(contents)
        file_wrt.close()


    # Main method to modify the sourcecode of a latex file to color each cell of a table in a different RGB
    # value and remove comments. Compiles latex outputting a PDF, then creates PNG's of the colored and
    # original pages for further utilisation.
    # @param: String path to the folder containing the tex file to edit.
    # @param: String path to the folder containing the tex file to compile.
    # @return: String path to the output folder of the PNG files.
    def modify_doc(self):

        self.has_table = False

        # Get paths that will be used

        edit_path = self.path_e

        compile_path = self.path_c

        f_compile = open(compile_path, "r", encoding="utf8", errors='ignore' )

        # Then get the content of the document to be compiled

        comp_contents = self.get_contents(f_compile)

        f_compile.close()

        ##Add the package to the file to be compiled

        comp_contents = self.add_package(comp_contents)

        # Write the edited main content into the source

        self.write_doc(compile_path, comp_contents)

        # Get the content of the document to be edited

        # Remove the comments from the contents

        f_edit = open(edit_path, "r", encoding="utf8", errors='ignore' )
        contents = self.get_contents(f_edit)
        f_edit.close()

        contents = self.rem_comment(contents)

        #Save original contents for later
        orig_contents = contents

        for j in range(2):
            i = 0
            is_Black = True
            out = []
            first_index = 0
            color_flag = "original"

            #In the second run, textcolor values will be varying rgb values instead of black
            if j is 1:
                is_Black = False
                color_flag = "colored"
                contents = orig_contents

            #Start the first splitting&editing process starting from the first table
            out = self.split_tabular(contents, i , is_Black)
            first_index = out[0]
            contents = out[1]

            if self.has_table is False:
                print("NO TABLES")
                return None

            #Iteratively split&edit all tables in the document.
            while i < len(contents) and (first_index < i or i == 0):
                if i == 0:
                    i = first_index
                out = self.split_tabular(contents,i,is_Black)
                i = out[0]
                contents = out[1]


            #Write the edited content into the source

            self.write_doc(edit_path, contents)

            #Compile the whole document

            paths = self.compile_latex(compile_path)

            if paths is None:
                return None

            out_path, go_path, pdf_path = self.split_paths(paths)

            self.to_png(out_path, go_path, pdf_path, color_flag)

        return out_path
