import os


class UnarchUtil:
    """
    A class to run the command line script for the unarchival of the arxiv archive.
    :author: Cagla Sozen (Intern)
    :version: 1.2 (After run through)
    :date: 04.09.2019
    """

    def __init__(self, src_path):
        """
            Constructor for the UnarchUtil class. Initializes the class with path to the archived file
            and path to output the unarchived files.
            :param src_path: Path to the archived file.
            :param out_path: Path to output the unarchived files.
            """
        self.src_path = src_path


    def get_contents(self, f):
        """
        Method to get the content of a document using the file pointer.
        :param f: File pointer to the regarding file.
        :return: String contents of the document.
        """
        contents = ""

        if f.mode == 'r':
            contents = f.read()
            f.close()

        return contents

    def unarchive(self):
        """
        Functions like a main method for the class. Get the contents of the cmd script and executes those commmands.
        :return: Path to output the unarchived files.
        """
        f_script = open(self.src_path, "r")
        script = self.get_contents(f_script)
        os.system(script)