#!/usr/bin/env conda

import os

import json

import math

import re


class ExtractInfoUtil:
    """
    A class to extract general table information into a JSON document and extract the tables in a Latex document into a separate tex file.
    :author: Cagla Sozen (Intern)
    :version: 1.2 (After run through)
    :date: 04.09.2019
    """

    COMM_OFFSET = 3  # an offset to use when we need to detect comments and non comments (% or \\%)
    END_OFFSET = 15  # an offset to use when we need to skip to the end of the endtabular command

    tables = []

    SPECH_CHS = ['%', '$', '{', '}', '_', 'P', '#', '&', 'S', '^']

    def __init__(self, path):
        """
        Constructor for the ExtractInfoUtil class. Initializes the class with the path to the tex file editing.
        :param path: path to the tex file.
        """
        self.path = path
        self.path = path
        self.tables = []

    def split_tabular(self, contents, ind):
        """
         Method to extract the tables from the given latex document.
        :param contents: String contents of the document
        :param ind: int index to start the search from
        :return: int last evaluated index, to start the next function call from this index.
        """

        doc_ind = contents.find("\\begin{document}", ind)

        beg_ind = contents.find("\\begin{tabular}", ind)
        end_ind = contents.find("\\end{tabular}", ind) + self.END_OFFSET

        if ((beg_ind != -1) and (end_ind != -1)) and doc_ind < beg_ind:
            real_content = contents[beg_ind - self.COMM_OFFSET:end_ind] + "\n"
            real_content = self.rem_comment(real_content)

            if real_content.find("\\begin{tabular}") == -1:
                return end_ind

            self.tables.append(real_content + "\n")

        return end_ind

    def rem_comment(self, line):
        """
         Method to remove commented lines in a text according to Latex commenting rules.
        :param line: String line to remove the comments from
        :return: String line without commented lines
        """
        comm_st = line.find("%")
        comm_end = line.find("\n", comm_st)

        while comm_st != -1 and comm_end != -1:
            if line[comm_st - 1] == "\\":
                comm_st = line.find("%", comm_st + 1)
                continue

            line = line[:comm_st] + "\n" + line[comm_end + 1:]
            comm_st = line.find("%")
            comm_end = line.find("\n", comm_st)

        return line

    def create_directory(self, path):
        """
        Method to create a directory with the given path.
        :param path:String target path including the name of the folder to be opened.
        :return: path to the directory created
        """
        try:
            os.mkdir(path)
        except OSError:
            print("Unable to create directory")
        else:
            print("Created directory")

        return path

    def read_file(self, path):
        """
         Method to read a file from the given path.
        :param path:  String target path including the name of the folder to be opened.
        :return: String content read from the file
        """
        f_desc = open(path, "r")

        if f_desc.mode == 'r':
            contents = f_desc.read()
            f_desc.close()
        return contents


    def get_table_info(self, table):
        """
        Method to compute table size.
        :param table: String table to evaluate
        :return: int Tuple number of rows and columns in the table
        """
        col_no = 0
        row_no = 0

        first_row = table.find("\\\\")

        first_col = table.find("&")

        while first_row != -1:
            row_no = row_no + 1
            first_row = table.find("\\\\", first_row + 1)

        while first_col != -1:
            col_no = col_no + 1
            first_col = table.find("&", first_col + 1)

        col_no = col_no + row_no
        col_no = math.ceil(col_no / row_no)

        return row_no, col_no


    def clean_cell(self, cell_val):
        """
        Method to clean the content of a cell for extracting the data in the cell that will be useful for detection tasks.
        :param cell_val:  String table to evaluate
        :return: boolean to indicate whether the cell needs further cleaning or not, string the new content after the cleaning
        """

        is_clean = False

        spec_flag = cell_val.find("\\")
        tag_flag = re.search("\\\\[0-9a-zA-Z_,/%!'^+&()=\.-]+\s*", cell_val)
        var_flag = re.search("(\$.*?\$)", cell_val)

        if spec_flag == -1 and tag_flag is None and var_flag is None:
            is_clean = True
            return is_clean, cell_val

        if var_flag is not None:
            var_ind = var_flag.span()
            var_st = var_ind[0]
            var_end = var_ind[1]
            cell_val = cell_val[var_st + 1:var_end - 1] + " " + cell_val[var_end:]
            return is_clean, cell_val

        if tag_flag is not None:
            indeces = tag_flag.span()
            end = indeces[1]
            begin = indeces[0]

            cell_val = cell_val[:begin] + cell_val[end:]
            return is_clean, cell_val

        if spec_flag != -1:
            next_char = cell_val[spec_flag + 1]
            if next_char in self.SPECH_CHS:
                cell_val = cell_val[:spec_flag] + cell_val[spec_flag + 1:]
                return is_clean, cell_val
            else:
                is_clean = True
                return is_clean, cell_val

        return is_clean, cell_val


    def get_table_info_detail(self, table):
        """
        Method to compute number of cells in each row of a table and the rgb values.
        :param table: String table to evaluate
        :return:  List containing table information.
        """

        row_list = []
        cols = []
        colors = []
        row_no = 0
        first_code = 0

        first_row = table.find("\\\\")
        first_col = table.find("&")

        prev = first_row
        prev_col = first_col

        while first_row != -1:
            row_no = row_no + 1
            cols = []
            flag = 0

            while first_col != -1 and first_col < first_row:
                tag = "textcolor\[RGB\]{.*?}"
                rgb_content = re.search(tag, table[first_code:])
                if rgb_content is not None:
                    indeces = rgb_content.span()
                    cont_string = rgb_content.group(0)
                    first_code = indeces[1] + len(table[:first_code])
                    start_val = cont_string.rfind("{")
                    colors = []
                    for i in range(3):
                        cont_string = cont_string[start_val:]
                        numbers = re.search("[0-9]+", cont_string)
                        colors.append(numbers[0])
                        start_val = numbers.span()
                        start_val = start_val[1] + 1
                colors = tuple(colors)

                cell_val = re.search("{.*?}", table[first_code:])
                cell_val_ind = cell_val.span()
                cell_val = cell_val.group(0)
                cell_val = cell_val[1:cell_val_ind[1] - 1]

                is_clean = False

                while is_clean is False:
                    is_clean, cell_val = self.clean_cell(cell_val)

                cell_val_info = {}
                cell_val_info['cell_val_info'] = cell_val

                colors_info = {}
                colors_info['colors_info'] = colors

                cell_cont = (colors_info, cell_val_info)

                cell_cont_info = {}
                cell_cont_info['cell_info'] = cell_cont

                cols.append(cell_cont)
                prev_col = first_col
                first_col = table.find("&", first_col + 1)

                if (first_col == -1 or first_col < first_row) and flag == 0:
                    if table.find("\\\\", first_col + 1) != -1:
                        first_col = prev_col
                        first_code = first_col
                        flag = 1

            row_no_info = {}
            row_no_info['row_no_info'] = row_no

            cells_info = {}
            cells_info['cells_info'] = cols

            cell_tuple = (row_no_info, cells_info)
            row_list.append(cell_tuple)
            prev = first_row
            first_row = table.find("\\\\", first_row + 1)

        return row_list

    def get_header_info(self, table):
        """
        Method to get the header info in a table.
        :param table: String table to evaluate
        :return: Dictionary to represent the headers and their existence. 1 means the corresponding header exists, 0 means doesn't.
        """

        header = "header"
        rule_dict = {}

        if table.find("\\hline") != -1:
            rule_dict.update({header: True})
        else:
            rule_dict.update({header: False})

        return rule_dict


    def to_json(self, dictionary, path):
        """
        Method to dump a python dictionary from a path to a json file
        :param dictionary: Dictionary to dump into JSON
        :param path: String path of the JSON file to dump the table info in
        :return: None
        """
        with open(path, 'w') as fp:
            json.dump(dictionary, fp, indent=4)


    def split_path(self, path):
        """
         Method to split paths that can be utilized in the code for outpur paths etc. For avoiding code duplication.
        :param path: String path to split into pieces
        :return:  String path to the outer directory of the given path, String filename from the path
        """
        dir_ind = path.rfind('/')
        name_ind = path.rfind('.')

        dir_path = path[:dir_ind + 1]
        filename = path[dir_ind + 1:name_ind]

        return dir_path, filename


    def extract_info(self):
        """
        Functions like a main function to call the above functions in order to extract tables and table information.
        Outputs in ../out folder, a .tex file with tables extracted and a .json file with document info
        :return: path to the json file outputted.
        """

        i = 0

        contents = self.read_file(self.path)

        ## TABLE CODES IN THE DOCUMENT IS EXTRACTED AND IF THERE ARE COMMENTS THEY ARE REMOVED ##

        first_index = self.split_tabular(contents, i)

        while i < len(contents) and (
                first_index < i or i == 0):  ##iterating through the contents of the document until all tables are extracted.
            if i == 0:
                i = first_index
            i = self.split_tabular(contents,
                                   i)  ##calling the function for each table with a new starting index to avoid finding the same tables at each iteration.

        outpath, filename = self.split_path(self.path)

        ## TABLES ARE WRITTEN IN A NEW TEX FILE IN AN "OUT" FOLDER TO BE CREATED ##

        dir_path = self.create_directory(outpath + "out")  ##creating a directory to store code outputs

        wrt_path = dir_path + "/" + filename + "_tables" + ".tex"

        file_wrt = open(wrt_path, "w+")  # we write the tables into a tex file in the original form

        for table in self.tables:
            file_wrt.write(table)  # write each table
        file_wrt.close()

        ## INFORMATION ABOUT EACH TABLE IS WRITTEN TO A DICTONARY ##

        dict_array = []  # same sized array with tables array. Only for this one, we will have dictionaries at each index.
        i = 0

        for table in self.tables:  # do all of the operations for each table
            info = self.get_table_info(table)

            rows = self.get_table_info_detail(table)
            header = self.get_header_info(table)

            header_info = {}
            header_info['header_info'] = header

            size_info = {}
            size_info['size_info'] = info

            rows_info = {}
            rows_info['rows_info'] = rows

            table_info = []
            table_info.append(size_info)
            table_info.append(rows)

            table_info.append(header)  # Last 3 dictionary elements is info upon headers.

            dict_array.append(table_info)  # append the dictionary to the array of all table information in the document
            i = i + 1

        tables_info = {}
        tables_info['tables_info'] = dict_array

        table_no_info = {}
        table_no_info['table_no'] = i

        tables_all = (table_no_info, tables_info)

        ## COMPLETE DOCUMENT INFORMATION IS WRITTEN ON A JSON FILE ##

        wrt_path = dir_path + "/" + filename + "_tables" + ".json"

        self.to_json(tables_all, wrt_path)
        return wrt_path
