"""
A class to run the scripts in an orderly fashion and such that they will pass inputs&outputs with each other.
:author: Cagla Sozen (Intern)
:version: 1.2 (After run through)
:date: 04.09.2019
"""

#Importing Python utility packages
import time

import glob

import os

#Importing custom scripts as modules
from src.unarchUtil import UnarchUtil

from src.modifyTableTextNew import ModifyTextUtilNew

from src.extractTableInfo import ExtractInfoUtil

from src.detectTable import DetectTableUtil

from src.toCOCO import CocoUtil

from src.extractRelation import ExtractRelationUtil


ARXIV_UNARCH_PATH = "/home/cagla/PycharmProjects/editDocument/sources/test/arxiv/unarch"
OUT_PATH = "/home/cagla/PycharmProjects/editDocument/out/"

scp_path = "/home/cagla/PycharmProjects/editDocument/src/unarchScript"
tot_time = 0
no_of_succ = 0

unarch_tool = UnarchUtil(scp_path)
unarch_tool.unarchive()

try:
    os.mkdir(OUT_PATH)
except OSError:
    print("Unable to create directory")
else:
    print("Created directory")

#Due to the document structure, we need to get into each folder in the unarchived folders, and then get the tex files.
path_arr = glob.glob(ARXIV_UNARCH_PATH + '/*/*.tex')

for path in path_arr:
    start_time = time.time()  #For time estimation of each document

    mod_tool = ModifyTextUtilNew(path, path)

    img_path = mod_tool.modify_doc()

    if img_path is None : #If the compilation was errorenous or there were no tables in the document, skip the path
        print("--Skipping Document--")
        continue

    extract_info_tool = ExtractInfoUtil(path)
    extract_info_tool.extract_info()

    detect_tool = DetectTableUtil(img_path)
    json = detect_tool.detect_table()

    coco_tool = CocoUtil(json)
    coco = coco_tool.to_coco()

    extract_rel_tool = ExtractRelationUtil(coco, json)
    out = extract_rel_tool.extract_relations()

    cur_time = (time.time() - start_time)
    print("--- %s seconds ---" % cur_time)  #For time estimation of each document
    os.rename(out, OUT_PATH + str(no_of_succ))
    tot_time = tot_time + cur_time
    no_of_succ = no_of_succ + 1
avg_time = tot_time / no_of_succ
print("--- Average time %s seconds ---" % avg_time)  #For time estimation of each document
