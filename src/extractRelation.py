import json


class ExtractRelationUtil:
    """
    A class to extract relations of table elements with each other and the table itself.
    :author: Cagla Sozen (Intern)
    :version: 1.2 (After run through)
    :date: 04.09.2019
    """
    UNCERTAINTY = 5

    objects = []
    tables = []
    prev_arr = set()
    relations = []
    predicate = ""
    rel_flag = False
    rel_id = 0

    def __init__(self, coco_path, json_path):
        self.coco_path = coco_path
        self.json_path = json_path
        self.objects = []
        self.tables = []
        self.prev_arr = set()
        self.relations = []
        self.predicate = ""
        self.rel_flag = False
        self.rel_id = 0

    def from_json(self, path):
        """
        Method to decode JSON data into Python format.
        :param path: String path to the JSON file
        :return: Serializable Python Data Type obtained from the JSON file
        """
        with open(path, 'r') as fP:
            dicts = json.load(fP)
        return dicts

    def to_json(self, dictionary, path):
        """
         Method to encode Python data into JSON data.
        :param dictionary:Serializable Python Data Type to encode
        :param path: String path to the JSON file
        """
        with open(path, 'w') as fp:
            json.dump(dictionary, fp, indent=4)

    def split_path(self, path):
        """
        Method to split paths that can be utilized in the code for output paths etc. For avoiding code duplication.
        :param path: String path to split into pieces
        :return: String path to the outer directory of the given path, String filename from the path
        """
        dir_ind = path.rfind('/')
        name_ind = path.rfind('.')

        dir_path = path[:dir_ind + 1]
        filename = path[dir_ind + 1:name_ind]

        return dir_path, filename

    def coord_to_int(self, obj, sub):
        """
        Method to convert coordinate read from files as Strings into int.
        :param obj: List information of relation object
        :param sub: List information of relation subject
        :return: Tuple of ints for x_coordinate of the object, y_coordinate of the object, x_coordinate of the subject, y_coordinate of the subject.
        """

        obj_x = obj[2]
        obj_x = int(obj_x['x'])
        obj_y = obj[3]
        obj_y = int(obj_y['y'])
        sub_x = sub[2]
        sub_x = int(sub_x['x'])
        sub_y = sub[3]
        sub_y = int(sub_y['y'])

        return obj_x, obj_y, sub_x, sub_y

    def is_next_row(self, obj, sub):
        """
        Method to check if the subject is in the next row of the object
        :param obj:  List information of relation object
        :param sub: List information of relation subject
        :return:  Boolean True if subject is in the next row, False otherwise.
        """

        obj_x, obj_y, sub_x, sub_y = self.coord_to_int(obj, sub)

        if abs(obj_y - sub_y) < self.UNCERTAINTY and obj_x < sub_x:
            return True
        return False

    def is_next_col(self, obj, sub):
        """
        Method to check if the subject is in the next column of the object
        :param obj:  List information of relation object
        :param sub: List information of relation subject
        :return: Boolean True if subject is in the next column, False otherwise.
        """

        obj_x, obj_y, sub_x, sub_y = self.coord_to_int(obj, sub)

        if abs(obj_x - sub_x) < self.UNCERTAINTY and obj_y < sub_y:
            return True
        return False


    def is_prev_row(self, obj, sub):
        """
        Method to check if the subject is in the previous row of the object
        :param obj: List information of relation object
        :param sub: List information of relation subject
        :return: Boolean True if subject is in the previous row, False otherwise.
        """

        obj_x, obj_y, sub_x, sub_y = self.coord_to_int(obj, sub)

        if abs(obj_y - sub_y) < self.UNCERTAINTY and obj_x > sub_x:
            return True
        return False

    def is_prev_col(self, obj, sub):
        """
        Method to check if the subject is in the previous column of the object
        :param obj:  List information of relation object
        :param sub:  List information of relation subject
        :return: Boolean True if subject is in the previous column, False otherwise.
        """

        obj_x, obj_y, sub_x, sub_y = self.coord_to_int(obj, sub)

        if abs(obj_x - sub_x) < self.UNCERTAINTY and obj_y > sub_y:
            return True
        return False

    def is_header_for(self, obj):
        """
        Method to check if the object is a header for the table.
        :param obj: List information of relation object
        :return: Boolean True if subject is a header for the table, False otherwise.
        """
        tbl_info = self.get_table_info_from_obj(obj)
        obj_id = obj[4]
        obj_id = obj_id['id']
        has_head = tbl_info[0]
        has_head = has_head['header']
        if self.has_prev(obj_id) is False and has_head is True:
            return True
        return False

    def has_prev(self, obj):
        """
        Method to check if the object has a previous relation.
        :param obj:  List information of relation object
        :return: Boolean True if subject has a previous relation, False otherwise.
        """
        if obj in self.prev_arr:
            return True
        return False

    def get_table_info_from_obj(self, obj):
        """
        Method to get the information about the table that the given object is contained in.
        :param obj:  List information of relation object
        :return: List  of table information. tbl_head denotes the header information of the table, page_inf denotes the
        page number that the table in contained in, and tbl no denotes the index of the table.
        """
        obj_id = obj[4]
        tbl_no = 0

        for table in self.tables['tables_info']:
            tbl_no = tbl_no + 1
            rows = table[1]
            for row in rows:
                cells = row[1]
                for cell in cells['cells_info']:
                    c_id = cell[3]
                    if c_id == obj_id:
                        tbl_head = table[2]
                        page_inf = table[5]

                        return tbl_head, page_inf, tbl_no

    def extract_relations(self):
        """
        Functions like a main method to extract the relations between cells from the JSON file and the COCO
        file outputted by the previously called scripts.
        :return: String path to the COCO file
        """

        coco_dicts = self.from_json(self.coco_path)

        json_dicts = self.from_json(self.json_path)
        self.tables = json_dicts[1]

        # Extract Objects
        for annotation in coco_dicts:
            height = {}
            width = {}
            x_coord = {}
            y_coord = {}
            id = {}
            annotation = annotation['annotation']
            cat_id = annotation['category_id']

            if cat_id == 3:
                bbox = annotation['bbox']
                id['id'] = annotation['id']
                height['height'] = bbox[3]
                width['width'] = bbox[2]
                x_coord['x'] = bbox[0]
                y_coord['y'] = bbox[1]

                obj_annotation = height, width, x_coord, y_coord, id

                self.objects.append(obj_annotation)

        dir_path, filename = self.split_path(self.json_path)
        wrt_path = dir_path + "objects.json"

        self.to_json(self.objects, wrt_path)

        # Extract Relations
        j = 0

        while (j < len(self.objects)):
            self.rel_flag = False
            k = 0

            obj = self.objects[j]

            obj_h = obj[0]
            obj_h = obj_h['height']

            if obj_h == None:
                j = j + 1
                continue

            while (k < len(self.objects)):

                if k == j:
                    k = k + 1
                    continue

                self.rel_flag = False

                sub = self.objects[k]
                k = k + 1

                sub_h = sub[0]
                sub_h = sub_h['height']

                if sub_h == None:
                    continue

                tbl_info_obj = self.get_table_info_from_obj(obj)
                tbl_info_sub = self.get_table_info_from_obj(sub)

                if (tbl_info_sub[2] != tbl_info_obj[2]) or (tbl_info_sub[1] != tbl_info_obj[1]):
                    continue

                if self.is_next_row(obj, sub) is True:
                    self.predicate = "nextInRow"
                    self.rel_flag = True

                elif self.is_next_col(obj, sub) is True:
                    self.predicate = "nextInColumn"
                    sub_id = sub[4]
                    sub_id = sub_id['id']
                    self.prev_arr.add(sub_id)
                    self.rel_flag = True

                elif self.is_prev_row(obj, sub) is True:
                    self.predicate = "prevInRow"
                    self.rel_flag = True

                elif self.is_prev_col(obj, sub) is True:
                    self.predicate = "prevInColumn"
                    self.rel_flag = True

                if self.rel_flag is False:
                    continue

                self.rel_id = self.rel_id + 1
                rel_annotation = obj, sub, self.predicate, self.rel_id

                self.relations.append(rel_annotation)
            j = j + 1

        for obj in self.objects:
            if self.is_header_for(obj) is True:
                predicate = "headerFor"
                self.rel_id = self.rel_id + 1
                rel_annotation = obj, obj, predicate, self.rel_id
                self.relations.append(rel_annotation)

        dir_path, filename = self.split_path(self.json_path)
        wrt_path = dir_path + "relations.json"

        self.to_json(self.relations, wrt_path)

        return dir_path
