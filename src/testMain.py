import time
import sys

#Importing custom scripts as modules

from src.modifyTableTextNew import ModifyTextUtilNew

from src.extractTableInfo import ExtractInfoUtil

from src.detectTable import DetectTableUtil

from src.toCOCO import CocoUtil

from src.extractRelation import ExtractRelationUtil


path = "/home/cagla/PycharmProjects/editDocument/sources/latex/unarch/1710.02784./SoccerBallPaper.tex"

mod_tool = ModifyTextUtilNew(path, path)

img_path = mod_tool.modify_doc()

if img_path is None : #If the compilation was errorenous or there were no tables in the document, skip the path
     print("--Skipping Document--")
     sys.exit(1)

extract_info_tool = ExtractInfoUtil(path)
json = extract_info_tool.extract_info()


detect_tool = DetectTableUtil(img_path)
json = detect_tool.detect_table()

coco_tool = CocoUtil(json)
coco = coco_tool.to_coco()

extract_rel_tool = ExtractRelationUtil(coco, json)
extract_rel_tool.extract_relations()
