#!/usr/bin/env conda

import re

import os

import random

from pdf2image import convert_from_path

from shutil import copyfile


class ModifyTextUtilNew:
    """
    A class to color the table text in a Latex document in a different RGB value for each color. This process also removes the comments in a document. Then recompiles latex, obtaines the PDF and exports to JPEG and PNG formats.
    :author: Cagla Sozen (Intern)
    :version: 1.2 (After run through)
    :date: 04.09.2019
    """

    OFFSET = 15  # Offset for skipping the end document tag
    LIB_TAG = "\\usepackage{xcolor} \n "

    red_perc = 255
    green_perc = 255
    blue_perc = 255
    has_Table = False  # Flag to check whether the document contains a table or not.
    beg_doc_ind = -1
    is_table = False
    orig_work_dir = ""

    def __init__(self, path_e, path_c):
        """
        Constructor for the ModifyTextUtilNew class. Initializes the class with compliation path and editing path.
        :param path_e: Path to the tex file to be edited.
        :param path_c: Path to the tex file to be compiled.
        """

        self.path_e = path_e
        self.path_c = path_c
        self.orig_work_dir = os.getcwd()
        self.red_perc = 255
        self.green_perc = 255
        self.blue_perc = 255
        self.has_Table = False  # Flag to check whether the document contains a table or not.
        self.beg_doc_ind = -1
        self.is_table = False


    def get_contents(self, f):
        """
        Method to get the content of a document using the file pointer.
        :param f: File pointer to the regarding file.
        :return: String contents of the document.
        """

        contents = ""

        if f.mode == 'r':
            contents = f.readline()
            if contents is None and 1 <= len(contents):
                print("File Finished")
                return None
        return contents

    def add_package(self, f, f_new, compile_path):
        """
        Method to add the necessary packages to the main latex source code.
        :param f: filepointer to the document
        :param f_new: filepointer to the document to append the edited lines.
        :param compile_path: path to the original document used for opening the file again.
        :return: index of the begin{document tag}
        """

        if self.has_package(f) is True:
            return

        f = open(compile_path, "r", encoding="utf8", errors='ignore')

        first_line = f.readline()
        second_line = f.readline()

        beg_ind_first = -1
        beg_ind_second = -1

        while first_line != None and 1 <= len(first_line):
            beg_ind_first = first_line.find("\\begin{document}")

            if second_line != None and 1 <= len(second_line):
                beg_ind_second = second_line.find("\\begin{document}")

            if beg_ind_second != -1:
                second_line = second_line[:beg_ind_second] + self.LIB_TAG + second_line[beg_ind_second:]
            self.append_doc(f_new, first_line)

            first_line = second_line
            second_line = f.readline()

        if beg_ind_first != -1:
            return beg_ind_first
        if beg_ind_second != -1:
            return beg_ind_second
        return -1


    def has_package(self, f):
        """
        A function to check whether the document contains the package command already or not
        :param f:  filepointer to the document
        :return: Boolean according to the existence of the package inclusion command.
        """

        line = f.readline()
        beg_doc = -1
        line_cnt = 1
        while line is not None and 1 <= len(line):
            if beg_doc != -1:
                f.close()
                self.beg_doc_ind = line_cnt
                if beg_ind == -1:
                    return False
                return True
            beg_ind = line.find("\\usepackage{xcolor}")

            line = f.readline()
            beg_doc = line.find("\\begin{document}")
            line_cnt = line_cnt + 1

        f.close()
        return False


    def rem_comment(self, line):
        """
        Removes comments if there is any from a line
        :param line: line to be evaluated
        :return: edited line
        """
        comm_st = line.find("%")

        if comm_st == -1:
            return line

        while comm_st != -1:
            if 1 <= comm_st:
                if line[comm_st - 1] == "\\":
                    comm_st = line.find("%", comm_st + 1)
                    continue
                else:
                    return line[:comm_st] + "\n"
            else:
                return line[:comm_st] + "\n"
        return line

    def compile_latex(self, path):
        """
         Method to compile the latex file in a given path.
        :param path: String path of the file to be compiled.
        :return: String tuple, the first is the path to the compiled tex file and the second is the path to the directory holding the main tex file.
        """

        ind = path.rfind("/")
        go_path = path[:ind]

        path = path[ind + 1:]

        for i in range(2):
            os.chdir(go_path)
            ex = os.system("pdflatex -halt-on-error -interaction=nonstopmode " + path + " >/dev/null ")
            os.chdir(self.orig_work_dir)
            if ex != 0:
                print("LATEX ERROR")
                return None
        return path, go_path


    def to_png(self, out_path, go_path, pdf_path, name):
        """
         Method to convert all pages in a PDF document into PNG.
        :param out_path: String path to the output directory
        :param go_path: String path to the hosting directory
        :param pdf_path: String name of the output files.
        :param name: String name to be given to the outputted PNG files.
        :return: int 1 for success, -1 for failure
        """
        p_no = 0

        try:
            os.mkdir(out_path)
        except OSError:
            print("Unable to create directory %s" % out_path)
        else:
            print("Created directory %s " % out_path)

        out_path = go_path + "/png"
        try:
            pages = convert_from_path(go_path+ "/" +pdf_path, 500)
        except MemoryError:
            print("Memory error for PNG conversion")
            return -1
        for page in pages:
            print("Writing...")
            f_name = out_path + "/" +  name + "-" + str(p_no) + ".png"
            page.save(f_name, 'PNG')
            p_no = p_no + 1
        return 1

    def has_table(self, f):
        """
        A function to check whether the document contains a table or not. Ant the contained table has to be before begin{document} tag.
        :param f: file pointer to the file to be evaluated
        :return: Boolean, True if the document contains a table, else False
        """
        line = f.readline()
        beg_ind = -1
        line_cnt = 1
        while line is not None and 1 <= len(line):
            beg_ind = line.find("\\begin{tabular}")
            if beg_ind != -1 and self.beg_doc_ind < line_cnt:
                f.close()
                return True
            line = f.readline()
            line_cnt = line_cnt + 1

        f.close()
        return False

    def modify_cells(self, line):
        """
        A function to edit the contents of a cell in a given line.
        :param line: String line to be edited
        :return: String edited version of the line
        """
        i = 0
        mod_line = ""
        add = 1

        while re.search("(.+?&)|((.+\\\\))\s", line) is not None:

            real_content = re.search("(.+?&)|((.+\\\\))\s", line)
            if (real_content is None):
                continue
            indeces = real_content.span()
            cont_string = real_content.group(0)

            end  = indeces[1]
            begin = indeces[0]

            if re.search("&", cont_string) is not None:
                add = 1
            else:
                add = 3

            ex_bra_left = cont_string.find("{")
            ex_bra_right = cont_string.find("}")

            norm = line.find(cont_string)

            while ex_bra_left != -1 and ex_bra_right != -1:
                begin = norm + ex_bra_left +1
                end =  norm + ex_bra_right + 1

                ex_bra_left = cont_string.find("{", ex_bra_left+1)
                ex_bra_right = cont_string.rfind("}", ex_bra_right-1)

            if self.is_black is True:
                tag_perc = str(0) + "," + str(0) + "," + str(0)
            else:
                self.red_perc = (self.red_perc - random.randint(1, 10)) % 255
                self.blue_perc = (self.blue_perc - random.randint(1, 10)) % 255
                self.green_perc = (self.green_perc - random.randint(1, 10)) % 255
                tag_perc = str(self.red_perc) + "," + str(self.blue_perc) + "," + str(self.green_perc)


            colortag_beg = "\\textcolor[RGB]{" + tag_perc + "}{"
            colortag_end = "} "

            mod_line = mod_line + line[:begin] + colortag_beg + line[begin:end - add] + colortag_end + line[end-add:end + 1]

            line = line[(end + 1):]
            self.is_table = True

        return mod_line + line

    def split_paths(self, paths):
        """
        Method to split parts of a given path.
        :param paths:  String path to be splitted.
        :return: String list, the first is path to the png output path, second is the path to the hosting directory,
         last one is the path to the pdf created from latex compilation.
        """
        path = paths[0]
        go_path = paths[1]

        new_ind = path.rfind(".")

        pdf_path = path[:new_ind] + ".pdf"

        out_path = go_path + "/png"

        return out_path, go_path, pdf_path

    def write_doc(self, path, contents):
        """
        Method to write the given contents to a document in the given path.
        :param path: String path to the document to be written on.
        :param contents: String contents to be edited.
        :return: None
        """
        file_wrt = open(path)

        file_wrt.write(contents)
        file_wrt.close()

    def open_doc(self, path):
        """
        Method to open a document in appending mode. If file doesn't exist, creates a new file in the given path.
        :param path: path to the file.
        :return: file pointer to the opened file.
        """
        f = open(path, "a+", encoding="utf8", errors='ignore')
        return f

    def append_doc(self, f, line):
        """
        Method to append a line to the end of the document.
        :param f: file pointer to the file to be appended.
        :param line: String line to be appended to the file.
        :return: None
        """
        f.write(line)
        return

    def extract_path(self, path):
        """
        Method to extract the directory of a given file's path and it's name.
        :param path: String path to the file to be appended.
        :return: String array, the first index is path of the directory of the file, the second index is the name of the given file.
        """
        dir_ind = path.rfind('/')
        name_ind = path.rfind('.')

        dir_path = path[:dir_ind+1]
        filename = path[dir_ind+1:name_ind]

        return dir_path, filename

    def modify_doc(self):
        """
        Functions like a main method to modify the sourcecode of a latex file to color each cell of a table in a different RGB
        value and remove comments. Compiles latex outputting a PDF, then creates PNG's of the colored and
        original pages for further utilisation.
        :return:  String path to the output folder of the PNG files.
        """

        self.has_Table = False
        color_flag = "original"
        self.is_black = True
        # Get paths that will be used

        edit_path = self.path_e

        compile_path = self.path_c

        f_compile = open(compile_path, "r", encoding="utf8", errors='ignore')

        paths = self.extract_path(compile_path)

        compile_path_new = paths[0] + "new_file_compile.tex"

        f_comp_new = self.open_doc(compile_path_new)

        self.add_package(f_compile, f_comp_new, compile_path)

        f_compile.close()
        f_comp_new.close()

        paths = self.extract_path(compile_path)
        old_path = paths[0]

        os.rename(compile_path, old_path + "old_file_compile.tex")

        os.rename(compile_path_new, compile_path)


        ##CORRECT UNTIL HERE

        for i in range(2):

            if i == 0:
                copyfile(edit_path, old_path + "edit_original.tex")

            if i == 1:
                edit_path = self.path_e
                edit_path = old_path + "edit_original.tex"
                color_flag = "colored"
                self.is_black = False

            f_edit = open(edit_path, "r", encoding="utf8", errors='ignore')

            self.has_Table = self.has_table(f_edit)

            if self.has_Table is False:
                print("NO TABLES")
                return None

            paths = self.extract_path(edit_path)
            path_new = paths[0]

            # Then get the content of the document to be compiled
            f_edit = open(edit_path, "r", encoding="utf8", errors='ignore' )

            edit_contents = self.get_contents(f_edit)
            f_new = self.open_doc(path_new + "new_document.tex")

            while edit_contents is not None and 1 <= len(edit_contents):
                edit_contents = self.rem_comment(edit_contents)

                beg_ind = edit_contents.find("\\begin{tabular}")

                if beg_ind != -1:
                    self.append_doc(f_new, edit_contents)
                    edit_contents = self.get_contents(f_edit)
                    end_ind = edit_contents.find("\\end{tabular}")

                    while end_ind == -1:
                        edit_contents = self.rem_comment(edit_contents)
                        edit_contents = self.modify_cells(edit_contents)
                        self.append_doc(f_new, edit_contents)

                        edit_contents = self.get_contents(f_edit)
                        end_ind = edit_contents.find("\\end{tabular}")

                self.append_doc(f_new, edit_contents)
                edit_contents = self.get_contents(f_edit)

            f_edit.close()
            f_new.close()

            paths = self.extract_path(edit_path)
            old_path = paths[0]

            os.rename(path_new + "new_document.tex", edit_path)
            if i == 1:
                os.rename(edit_path, self.path_e)

            if self.is_table == False:
                print("--Skipping Document Non-Table Tabular --")
                return None

            for i in range(2):
                paths = self.compile_latex(compile_path)

            if paths is None:
                return None

            out_path, go_path, pdf_path = self.split_paths(paths)

            success = self.to_png(out_path, go_path, pdf_path, color_flag)
            if success == -1:
                return None
        return out_path

