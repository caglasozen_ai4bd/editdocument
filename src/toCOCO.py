# A class to convert the complete information about the table obtained after detectTable into MS-COCO format.
# @author: Cagla Sozen (Intern)
# @version: 1.1 (After Code Review)
# @date: 28.08.2019


import json

import numpy as np

class CocoUtil:
    """
    A class to convert the complete information about the table obtained after detectTable into MS-COCO format.
    :author: Cagla Sozen (Intern)
    :version: 1.2 (After run through)
    :date: 04.09.2019
    """
    IMAGE_ID = -1
    SEGMENT = None
    AREA = -1
    IS_CROWD = 0

    annot_id = 1
    all_annotations = []
    tables = []
    dicts = []

    def __init__(self, json_path):
        """
        Constructor for the CocoUtil class. Initializes the class with the path to the json path outputted by detectTable.
        :param json_path: path to the json file.
        """
        self.json_path = json_path
        self.annot_id = 1
        self.all_annotations = []
        self.tables = []
        self.dicts = []


    def from_json(self, path):
        """
         Method to decode JSON data into Python format.
        :param path: String path to the JSON file
        :return: Serializable Python Data Type obtained from the JSON file
        """
        with open(path, 'r') as fP:
            self.dicts = json.load(fP)
        return self.dicts

    def to_json(self, dictionary, path):
        """
         Method to encode Python data into JSON data.
        :param dictionary:  Serializable Python Data Type to encode
        :param path: String path to the JSON file
        """
        with open(path, 'w') as fp:
            json.dump(dictionary, fp, indent=4)

    def to_serializable(self, data):
        """
         Method to convert a non-serializable numpy int64 data to serializable Python format.
        :param data:  Numpy INT64 data to serialize
        :return: Serializable Python Data Type obtained from the numpy data
        """
        val = np.int64(data)
        data_serial = val.item()
        return data_serial

    def split_path(self, path):
        """
        Method to split paths that can be utilized in the code for outpur paths etc. For avoiding code duplication.
        :param path:  String path to split into pieces
        :return: String path to the outer directory of the given path, String filename from the path
        """
        dir_ind = path.rfind('/')
        name_ind = path.rfind('.')

        dir_path = path[:dir_ind+1]
        filename = path[dir_ind+1:name_ind]

        return dir_path, filename


    def to_coco(self):
        """
         Functions like a main method to convert the information in the JSON file created by the extractTableInfo script into MS-COCO format.
        :return:String path to the COCO file
        """

        dicts = self.from_json(self.json_path)

        self.tables = self.dicts[1]

        for table in self.tables['tables_info']:
            rows = table[1]
            columns = table[3]
            columns = columns['columns_location_info']

            for row in rows:
                cells = row[1]
                for cell in cells['cells_info']:
                    cat_id = 3
                    cell_loc = cell[2]
                    cell_loc = cell_loc['location_info']
                    temp_orig = cell_loc['origin']
                    bbox = [temp_orig[0], temp_orig[1], cell_loc['width'], cell_loc['height']]
                    annotation = {"id": self.annot_id, "image_id": self.IMAGE_ID, "category_id": cat_id, "segmentation": self.SEGMENT,
                                  "area": self.AREA, "bbox": bbox, "iscrowd": self.IS_CROWD}
                    annotation_dict = {}
                    annotation_dict['annotation'] = annotation
                    self.all_annotations.append(annotation_dict)
                    cell_id = {}
                    cell_id['id'] = self.annot_id
                    cell.append(cell_id)
                    self.annot_id = self.annot_id + 1
                row_info = row[2]
                cat_id = 2
                row_info = row_info['location_info']
                temp_orig = row_info[2]
                bbox = [temp_orig[0], temp_orig[1], row_info[1], row_info[0]]
                annotation = {"id": self.annot_id, "image_id": self.IMAGE_ID, "category_id": cat_id, "segmentation": self.SEGMENT,
                              "area": self.AREA, "bbox": bbox, "iscrowd": self.IS_CROWD}
                annotation_dict = {}
                annotation_dict['annotation'] = annotation
                self.all_annotations.append(annotation_dict)
                row_id = {}
                row_id['id'] = self.annot_id
                row.append(row_id)
                self.annot_id = self.annot_id + 1

            if columns is not None:
                for column in columns:
                    cat_id = 4
                    temp_orig = column['origin']
                    bbox = [temp_orig[0], temp_orig[1], column['width'], column['height']]
                    annotation = {"id": self.annot_id, "image_id": self.IMAGE_ID, "category_id": cat_id, "segmentation": self.SEGMENT,
                                  "area": self.AREA, "bbox": bbox, "iscrowd": self.IS_CROWD}
                    annotation_dict = {}
                    annotation_dict['annotation'] = annotation
                    self.all_annotations.append(annotation_dict)
                    column['id'] = self.annot_id
                    self.annot_id = self.annot_id + 1

            #HEADER DATA

            cat_id = 5
            head_info = table[2]

            if head_info['header'] is True:
                head_bbox = head_info['location_info']
                head_coords = head_bbox['origin']
                bbox = [head_coords[0], head_coords[1], head_bbox['width'], head_bbox['height']]

                annotation = {"id": self.annot_id, "image_id": self.IMAGE_ID, "category_id": cat_id, "segmentation": self.SEGMENT, "area": self.AREA,
                              "bbox": bbox, "iscrowd": self.IS_CROWD}
                annotation_dict = {}
                annotation_dict['annotation'] = annotation
                self.all_annotations.append(annotation_dict)
                head_info['id'] = self.annot_id
                self.annot_id = self.annot_id + 1

            #TABLE DATA

            cat_id = 1
            temp_bbox = table[len(table) - 2]
            temp_bbox = temp_bbox['location_info']
            temp_coords = temp_bbox['origin']
            bbox = [temp_coords[0], temp_coords[1], temp_bbox['width'], temp_bbox['height']]

            annotation = {"id": self.annot_id, "image_id": self.IMAGE_ID, "category_id": cat_id, "segmentation": self.SEGMENT, "area": self.AREA,
                          "bbox": bbox, "iscrowd": self.IS_CROWD}
            annotation_dict = {}
            annotation_dict['annotation'] = annotation
            self.all_annotations.append(annotation_dict)
            tab_id = {}
            tab_id['id'] = self.annot_id
            table.append(tab_id)
            self.annot_id = self.annot_id + 1

        # Write the updated dictionary into JSON
        dicts[1] = self.tables
        self.to_json(dicts, self.json_path)

        # Write the updated dictionary into JSON

        dir_path, filename = self.split_path(self.json_path)

        wrt_path = dir_path + "COCO.json"

        self.to_json(self.all_annotations, wrt_path)

        return wrt_path
