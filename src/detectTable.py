import glob

import cv2

import numpy as np

import matplotlib.pyplot as plt

import json

class DetectTableUtil:
    """
     A class to extract the location information of the found tables on the tex file, using the PNG images genereated by the "modifyTable" script.
    :author: Cagla Sozen (Intern)
    :version: 1.2 (After run through)
    :date: 04.09.2019
    """

    # Initialize variables who are going to be needed in the following lines.
    tables = []
    colors = []
    data = []
    dir_path = ""
    color_arr = []
    orig_arr = []
    table_rep = []
    rgb_val = [0, 0, 0]
    tbl_page_no = []
    image_parts = []
    diff_arr = []

    def __init__(self, img_path):
        """
        Constructor for the DetectTableUtil class. Initializes the class with the path to the folder containing the PNGs.
        :param path: path to the PNG folder created by modifyTableTextUtilNew.
        """
        self.img_path = img_path
        self.tables = []
        self.colors = []
        self.data = []
        self.dir_path = ""
        self.color_arr = []
        self.orig_arr = []
        self.table_rep = []
        self.rgb_val = [0, 0, 0]
        self.tbl_page_no = []
        self.image_parts = []
        self.diff_arr = []
    

    def from_json(self, path):
        """
        Method to decode JSON data into Python format.
        :param path: String path to the JSON file
        :return: Serializable Python Data Type obtained from the JSON file
        """
        with open(path, 'r') as fP:
            dicts = json.load(fP)
        return dicts


    def split_path(self, path):
        """
         Method to extract the directory of a given file's path and it's name.
        :param path: String path to the file to be appended.
        :return: String array, the first index is path of the directory of the file, the second index is the name of the given file.
        """
        dir_ind = path.rfind('/')
        name_ind = path.rfind('.')

        dir_path = path[:dir_ind+1]
        filename = path[dir_ind+1:name_ind]
    
        return dir_path, filename
    

    def to_json(self, dictionary, path):
        """
        Method to encode Python data into JSON data.
        :param dictionary:  Serializable Python Data Type to encode
        :param path:  String path to the JSON file
        """
        with open(path, 'w') as fp:
            json.dump(dictionary, fp, indent=4)
    

    def to_serializable(self, data):
        """
         Method to convert a non-serializable numpy int64 data to serializable Python format.
        :param data: Numpy INT64 data to serialize
        :return: Serializable Python Data Type obtained from the numpy data
        """
        val = np.int64(data)
        data_serial = val.item()
        return data_serial
    

    def get_loc_info(self, coord_list):
        """
         Method to get the origin, width, height information of an entity from a given coordinate list.
        :param coord_list: List of all coordinates of the entity
        :return: Tuple including topleft, bottom right coordinates and width,height of the entity
        """
        coord_tuples = []
    
        first = coord_list[1]  # Extracting the x-dimension into a separate array
        second = coord_list[0]  # Extracting the y-dimension into a separate array
    
        # Creating a tuples array that will represent the coordinates of the pixels in (x,y) form
        for i in range(len(first)):
            coord = [first[i], second[i]]
            coord_tuples.append(tuple(coord))
    
        # Getting the max and min values of both y and x values.
        max_values = list(map(max, zip(*coord_tuples)))
        min_values = list(map(min, zip(*coord_tuples)))
    
        x_coord = min_values[0], max_values[0]
        y_coord = min_values[1], max_values[1]
    
        # From the minimum and maximum values, height and width of the ROI can be obtained
        width = x_coord[1] - x_coord[0]
        height = y_coord[1] - y_coord[0]
    
        # Since numpy used int64 data format we need to convert it into a serializable format.
        x0 = self.to_serializable(x_coord[0])
        y0 = self.to_serializable(y_coord[0])
    
        x1 = self.to_serializable(x_coord[1])
        y1 = self.to_serializable(y_coord[1])
    
        height = self.to_serializable(height)
        width = self.to_serializable(width)
    
        # Origin and bottom right pixels of the image can be obtained using the minimum and maximum values once again.
        top_left = x0, y0
        bot_right = x1, y1
    
        return top_left, bot_right, height, width
    

    def get_col_info(self, array, col_size, dst):
        """
        Method to get the column coordinates of a given table.
        :param array: Array coordinates in the table
        :param col_size: int number of columns in the table wşth the given coordinates
        :param dst: size of the entity for utilising as a maximum value for the bottom left corner.
        :return: Dictionary of column info
        """

        col_vals = []
        col_dict = []
    
        for i in range(col_size):
            col_height = 0
            col_width = 0
            col_origin = (len(dst), len(dst))
            col_end = (0, 0)
    
            col_info = col_height, col_width, col_origin, col_end
            col_vals.append(col_info)
    
        for row in array:
            i = 0
            for column in row:
                cell_loc = column['location_info']
    
                cell_height = cell_loc['height']
                cell_width = cell_loc['width']
                cell_origin = cell_loc['origin']
                cell_end = cell_loc['end_coord']
    
                curr_col = col_vals[i]
    
                col_height = curr_col[0]
                col_width = curr_col[1]
                col_origin = curr_col[2]
                col_end = curr_col[3]
    
                if (cell_height is not None) and (col_height < cell_height):
                    col_height = cell_height
    
                if (cell_width is not None) and (col_width < cell_width):
                    col_width = cell_width
    
                if (cell_origin != (None, None)) and (cell_origin < col_origin):
                    col_origin = cell_origin
    
                if (cell_end != (None, None)) and (col_end < cell_end):
                    col_end = cell_end
    
                col_vals[i] = col_height, col_width, col_origin, col_end
                i = i + 1
    
        for column in col_vals:
            col_coords = {"height": column[0], "width": column[1], "origin": column[2], "end_coord": column[3]}
            col_dict.append(col_coords)
    
        return col_dict
    

    def detect_table(self):
        """
        Functions like a main method to get the location information of the tables and their entities from the images into a JSON file.
        :return:  String path to the output folder of the JSON file
        """
    
        # Get paths to the two images used
        path = self.img_path
    
        arr = glob.glob(path + '/colored-*.png')
    
        for pg in range(len(arr)):
            self.color_arr.append(path + '/colored-' + str(pg) + '.png')
            self.orig_arr.append(path + '/original-' + str(pg) + '.png')

        i = 0
        for i in range(len(self.color_arr)):
            file_colored = self.color_arr[i]
            file_original = self.orig_arr[i]
    
            im_colored = cv2.imread(file_colored, 1)
            im_original = cv2.imread(file_original, 1)
    
            if im_colored.shape != im_original.shape:
                continue
    
            im_diff = np.subtract(im_colored, im_original)
    
            im_diff_color = im_diff
    
            im_diff = cv2.cvtColor(im_diff, cv2.COLOR_BGR2GRAY)
    
            coord_list = np.nonzero(im_diff)
    
            if len(coord_list[0]) == 0 and len(coord_list[1]) == 0:
                continue
    
            self.dir_path, filename = self.split_path(path)

            self.dir_path, garb = self.split_path(self.dir_path)
    
            wrt_path = self.dir_path + "/" + filename + "_tables" + ".json"
    
            cv2.imwrite(self.dir_path + "/out/diff-" + str(i) + ".png", im_diff_color)
    
        # Get path to the JSON file holding table information
        json_path = self.dir_path + "out"
        out_path = json_path + "/"
    
        json_arr = glob.glob( json_path + '/*.json')
    
        json_path = json_arr[0]
        # Convert the JSON data into Pyton to use from this point on in the code
        dicts = self.from_json(json_path)
    
        tables_list = dicts[1]

        # Get data table by table from the JSON file into the variable "tables"
        # Iterate within the tables to get the color content of each cell later to be used to detect cells by colors
    
        for table in tables_list['tables_info']:
            rep = 0
            rows = table[1]
            for row in rows:
                cells = row[1]
                cell_no = len(cells['cells_info'])
                for cell in cells['cells_info']:
                    color_info = cell[0]
                    color_val = color_info['colors_info']

                    val_info = cell[1]
                    cell_val = val_info['cell_val_info']

                    if rep == 1:
                        if cell_val == " ":
                            rep = rep - 1
                        else:
                            self.table_rep.append(color_val)
                            break
                    rep = rep + 1
                if(rep == 1):
                    break
    
        self.diff_arr = glob.glob( out_path + '/*.png')
    
        for rep in self.table_rep:
            self.rgb_val[0], self.rgb_val[1], self.rgb_val[2] = rep
            b, g, r = int(self.rgb_val[2]), int(self.rgb_val[1]), int(self.rgb_val[0])
            for diff in self.diff_arr:
                im_diff = cv2.imread(diff, 1)
    
                mask = cv2.inRange(im_diff, (b, g, r), (b, g, r))
                out = cv2.bitwise_and(im_diff, im_diff, mask=mask)
    
                #Show the differences between the colored and original image
                # cv2.namedWindow('Dif', cv2.WINDOW_NORMAL)
                # cv2.resizeWindow('Dif', 600, 600)
                #
                # cv2.imshow("Dif", out)
                # cv2.waitKey(0)
                # cv2.destroyAllWindows()
    
                temp = cv2.cvtColor(out, cv2.COLOR_BGR2GRAY)
    
                coord_list = np.nonzero(temp)
    
                if len(coord_list[0]) == 0 and len(coord_list[1]) == 0:
                    continue
                tbl_page_no_end = diff.rfind(".")
                tbl_page_no_st = diff.rfind("-")
    
                self.tbl_page_no.append(diff[tbl_page_no_st + 1: tbl_page_no_end])
    
        tbl_ind = 0
    
        table = None
    
        for table in tables_list['tables_info']:
            rows = table[1]
            for row in rows:
                cells = row[1]
                for cell in cells['cells_info']:
                    color_info = cell[0]
                    color_val = color_info['colors_info']
                    tbl_no = self.tbl_page_no[tbl_ind]
                    cell_val = (color_val, tbl_no)
                    self.colors.append(cell_val)
            tbl_ind = tbl_ind + 1
    
        # Extract the Region of Interest(ROI) for each iteration using the color information obtained above
        for color in self.colors:
    
            self.rgb_val[0], self.rgb_val[1], self.rgb_val[2] = color[0]
    
            diff_path = out_path + "/diff-" + color[1] + ".png"
    
            im_diff = cv2.imread(diff_path, 1)
    
            b, g, r = int(self.rgb_val[2]), int(self.rgb_val[1]), int(self.rgb_val[0])
            mask = cv2.inRange(im_diff, (b, g, r), (b, g, r))
            out = cv2.bitwise_and(im_diff, im_diff, mask=mask)
    
            roi_inf = ()
            self.image_parts.append(out)
    
        dst = self.image_parts[0]
    
        # For each extracted ROI, obtain location information
        for dst in self.image_parts:
            coord_list = []
    
            dst = cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY)  # Convert to Gray in order to manipulate 2D arrays instead of 3D for ease of use.
    
            coord_list = np.nonzero(dst)  # Getting the nonzero parts of the array representing the image to obtain the pixel locations containing the ROI, since they will be the only pixels with a nonzero value.
    
            if len(coord_list[0]) != 0 and len(coord_list[1]) != 0:  # To avoid errors stemming from empty cells.
    
                top_left, bot_right, height, width = self.get_loc_info(coord_list)
    
                # Putting the data into a dictionary to write into JSON
                part_data = {"height": height, "width": width, "origin": top_left, "end_coord": bot_right}
            else:  # If the cell is empty, then it has 0 height, width and origin by default
                # Putting the data into a dictionary to write into JSON
                part_data = {"height": None, "width": None, "origin": (None, None), "end_coord": (None, None)}
    
            location_info = {}
            location_info['location_info'] = part_data
    
            self.data.append(location_info)
    
        # Add the new coordinate information into the original table information obtained from the JSON file in the beginning
        index = 0
        tbl_ind = 0
        header_flag = 0
        for table in tables_list['tables_info']:
            multline_flag = False
            rows = table[1]
            row_coords = []
            first_row = True
            header_flag = table[2]
            header_flag = header_flag['header']
    
            col_coords = []
            col_size = 0
    
            for row in rows:
                cells = row[1]
                prev_col_size = col_size
                col_size = len(cells['cells_info'])
    
                if (prev_col_size != 0 and col_size != prev_col_size):
                    multline_flag = True
    
            for row in rows:
                cells = row[1]
                all_row_coords = []
    
                row_height = 0
                row_width = 0
                row_origin = (len(dst), len(dst))
                row_end = (0, 0)
    
                col_size = len(cells['cells_info'])
    
                for cell in cells['cells_info']:
                    cell.append(self.data[index])
                    all_row_coords.append(self.data[index])
                    index = index + 1
    
                    cell_info = cell[2]
                    cell_info = cell_info['location_info']
                    cell_origin = cell_info['origin']
                    cell_height = cell_info['height']
                    cell_width = cell_info['width']
                    cell_end = cell_info['end_coord']
    
                    if (cell_height is not None) and (row_height < cell_height):
                        row_height = cell_height
    
                    if (cell_width is not None) and (row_width < cell_width):
                        row_width = cell_width
    
                    if (cell_origin is not (None, None)) and (cell_origin < row_origin):
                        row_origin = cell_origin
    
                    if (cell_end is not (None, None)) and (row_end < cell_end):
                        row_end = cell_end
    
                row_info = row_height, row_width, row_origin, row_end
    
                if (first_row is True and header_flag is True):
                    header_info = {"height": row_height, "width": row_width, "origin": row_origin, "end_coord": row_end}
                    table_header = table[2]
                    table_header['location_info'] = header_info
    
                col_coords.append(all_row_coords)
                row_coords.append(row_info)
                row_coords_info = {}
                row_coords_info['location_info'] = row_info
                row.append(row_coords_info)
                first_row = False
    
            # Calculate column information here[{'location_info': {'height': None, 'width': None, 'origin': (None, None), 'end_coord': (None, None)
    
            if (multline_flag is False):
                col_vals = self.get_col_info(col_coords, col_size,dst)
            else:
                col_vals = None
    
            col_coords_info = {}
            col_coords_info['columns_location_info'] = col_vals
            table.append(col_coords_info)
    
    
            # Row coordinates comparison should be done here
    
            zipped = zip(*row_coords)
            zipped = list(zipped)
            zipped_min = zipped[2]
            zipped_max = zipped[3]
    
            max_coords = list(map(max, zip(*zipped_max)))
            min_coords = list(map(min, zip(*zipped_min)))
    
            table_height = max_coords[0] - min_coords[0]
            table_width = max_coords[1] - min_coords[1]
            table_origin = min_coords
    
            table_coords = {"height": table_height, "width": table_width, "origin": table_origin}
    
            table_coords_info = {}
            table_coords_info['location_info'] = table_coords
    
            table_page_info = {}
            table_page_info['page_info'] = int(self.tbl_page_no[tbl_ind])
            tbl_ind = tbl_ind + 1
            table.append(table_coords_info)
            table.append(table_page_info)
    
        # Write the updated dictionary into JSON
        ind = json_path.rfind("/")
        end_ind = json_path.rfind(".")
    
        filename = json_path[ind + 1:end_ind]  # extracting the newfile name from the path
        wrt_path = self.dir_path + "/out/" + filename + "_COCO.json"
    
        self.to_json(dicts, wrt_path)
    
        return wrt_path
